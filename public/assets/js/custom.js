$( function() {

/*
    $("#header").load("header.html", function(){});

    $("#footer").load("footer.html");*/

    $(document).on("click", ".parent", function() {
        var $this = $(this);


        if ($this.data("status") == "off") {

            $(".parent").each(function () {
                if ($(this).data("status") == "on" && $(this) != $this) {
                    $(this).data("status", "off");

                    $(this).css({
                        "background" : "url('images/ac-bg.png') repeat-x center -50px",
                        "color" : "#000"
                    });
                }
            });

            $this.data("status", "on");

            $(this)
                .css(
                {
                    "background" : "url('images/ac-bg.png') repeat-x center top",
                    "color" : "#ffffff"
                });
        } else {
            $this.css({
                "background" : "url('images/ac-bg.png') repeat-x center -50px",
                "color" : "#000"
            });

            $this.data("status", "off");

        }


    });


    $(document).on("click", "#myTab li", function() {
        $('.oyunlar').jScrollPane({ autoReinitialise: true });
    });

    $(document).on("mouseenter", ".oyun", function() {
        var id = $(this).attr('id');

        $("#" + id +" .oyun-image").fadeIn()
            .animate({top:-44}, {duration: 200, queue: false});

        $("#" + id + " .play").fadeIn()
            .animate({bottom:0}, {duration: 200, queue: false});

    });

    $(document).on("mouseleave", ".oyun", function() {
        var id = $(this).attr('id');

        $("#" + id +" .oyun-image").slideDown()
            .animate({top:0}, {duration: 200, queue: false});

        $("#" + id + " .play").slideDown()
            .animate({bottom:-45}, {duration: 200, queue: false});
    });




    $("#marquee").marquee({
        duration: 20000,
        delayBeforeStart: 0,
        direction: 'left'
    });

    $('#marquee2').marquee({
        duration: 15000,
        gap: 50,
        delayBeforeStart: 0,
        direction: 'left'
    });

    $('#marquee3').marquee({
        duration: 12000,
        gap: 30,
        delayBeforeStart: 0,
        direction: 'left'
    });

    $("#breakingnews").BreakingNews({
        width           : 200,
        timer			: 3000,
        autoplay		: true,
        effect			: 'slide'

    });

    $("#breakingnews2").BreakingNews({
        width           : 200,
        timer			: 5000,
        autoplay		: true,
        effect			: 'slide'

    });
    $("#breakingnews3").BreakingNews({
        width           : 200,
        timer			: 8000,
        autoplay		: true,
        effect			: 'slide'

    });




    $(document).on("mouseenter", ".oyunlar", function() {
        var $scroll = $(".jspVerticalBar");
        var $jsp = $(".jspTrack");
        var $jspContainer = $(".jspContainer");

        if ($jspContainer[0].scrollHeight > $jspContainer.height()) {
            $scroll.animate({opacity: 0}, 'slow', function () {
                $(this)
                    .animate({opacity: 1}, 'slow');
            });

            $jsp.animate({opacity: 0}, 'slow', function () {
                $(this)
                    .animate({opacity: 1}, 'slow');
            });
        }
    });

    $(document).on("mouseleave", ".oyunlar", function() {
        var $scroll = $(".jspVerticalBar");
        var $jsp = $(".jspTrack");
        var $jspContainer = $(".jspContainer");

        if ($jspContainer[0].scrollHeight > $jspContainer.height()) {
            $scroll.animate({opacity: 1}, 'slow', function () {
                $(this)
                    .animate({opacity: 0}, 'slow');
            });

            $jsp.animate({opacity: 1}, 'slow', function () {
                $(this)
                    .animate({opacity: 0}, 'slow');
            });
        }
    });




    $(document).on("click", ".language button", function() {
        var navbarCollapse = $(".navbar-collapse");

        navbarCollapse.animate({ scrollTop: navbarCollapse[0].scrollHeight}, 1000);
    });
   $(document).on("click", "#btnLogin", function() {
        $( "#dialog-message" ).dialog("open");
    });

   

    $( "#dialog-message" ).dialog({
        resizable: false,
        autoOpen : false,
        modal: true,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
            }
        }
    });

    $(".ui-dialog-titlebar").hide();

    /**********************/


});





$(window).resize(function () {
    resizeContainerSides();
});

function resizeContainerSides () {

    $(".last24").css(
        {
            "left" : $("#logo").position().left
        });


    $(".oyunlar").jScrollPane({
        showArrows: true,
        horizontalGutter: 3,
        arrowScrollOnHover: true
    });


}

resizeContainerSides();

