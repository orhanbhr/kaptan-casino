<?php
/**
 * Created by PhpStorm.
 * User: OrhanBHR
 * Website: www.orhanbhr.com
 * Mail: this@orhanbhr.com
 * Date: 13.11.15
 * Time: 02:16
 */


namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function user_bank_detail()
    {
        return $this->hasOne('App\UserBank', 'id', 'user_bank');
    }

}