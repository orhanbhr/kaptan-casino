<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'balance_log';
}