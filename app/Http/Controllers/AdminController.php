<?php

namespace App\Http\Controllers;


use App\TransactionDetails;
use App\UserBank;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Validator;
use Input;

use App\Balance;
use App\User;
use App\Transaction;
use Auth;
use View;


class AdminController extends Controller
{


    /**
     * @return bool
     */
    public function permissionCheck()
    {
        if(Auth::check())
        {
            $user = Auth::getUser();
            if($user->user_type != 3)
                return false;
            else
                return true;
        }else
            return false;
    }

    /**
     * @return Redirect
     */
    public function index()
    {
        if($this->permissionCheck())
            return View::make('admin.index')
                ->with('user', Auth::getUser());
        else
            return redirect('/');
    }


    /**
     * @return Redirect
     */
    public function getUsers()
    {
        $users = User::all();

        if($this->permissionCheck())
            return View::make('admin.users')
                ->with('user', Auth::getUser())
                ->with('users', $users);
        else
            return redirect('/');
    }

    /**
     * @param $user_id
     * @return Redirect
     */
    public function getBalance($user_id)
    {

        $balance        = Balance::where('user_id', $user_id)->orderBy('id','DESC')->get();

        $balanceUser    = User::find($user_id);

        if($this->permissionCheck())
            return View::make('admin.balance')
                ->with('user', Auth::getUser())
                ->with('balances', $balance)
                ->with('balanceUser', $balanceUser);
        else
            return redirect('/');
    }

    /**
     * @return Redirect
     */
    public function getDeposits()
    {
        $deposits = TransactionDetails::orderBy('id','DESC')->with('user')->get();

        if($this->permissionCheck())
            return View::make('admin.deposits')
                ->with('user', Auth::getUser())
                ->with('deposits', $deposits);
        else
            return redirect('/');
    }

    /**
     * @param $deposit_id
     * @param $process
     * @return Redirect
     */
    public function setDeposits($deposit_id, $process)
    {
        if($this->permissionCheck()) {
            $deposit = TransactionDetails::find($deposit_id);

            if ($deposit->status == 0) {
                $user = User::find($deposit->user_id);

                if ($process == 1) {

                    $current_balance = $user->balance;
                    $new_balance = $current_balance + $deposit->amount;
                    $user->balance = $new_balance;
                    $user->save();

                    $balance = new Balance();
                    $balance->user_id = $user->id;
                    $balance->balance_type = 3;
                    $balance->balance_message = 'Para yatırma işleminiz onaylanmıştır. Eski Bakiye: ' . $current_balance . " Yeni Bakiye : " . $new_balance;
                    $balance->amount = $deposit->amount;
                    $balance->save();

                } elseif ($process == 2) {
                    $balance = new Balance();
                    $balance->user_id = $user->id;
                    $balance->balance_type = 3;
                    $balance->balance_message = 'Para yatırma işleminiz red edilmiştir.';
                    $balance->amount = $deposit->amount;
                    $balance->save();
                }
                $deposit->status = $process;
                $deposit->save();

                return redirect()->back();
            } else
                return redirect()->back();
        }else
            return redirect('/');
    }

    /**
     * @return Redirect
     */
    public function getDraws()
    {
        $draws = Transaction::orderBy('id','DESC')->with('user')->with('user_bank_detail')->get();

        if($this->permissionCheck())
            return View::make('admin.draws')
                ->with('user', Auth::getUser())
                ->with('draws', $draws);
        else
            return redirect('/');
    }

    /**
     * @param $draw_id
     * @param $process
     * @return Redirect
     */
    public function setDraw($draw_id, $process)
    {
        if($this->permissionCheck()) {
            $draw = Transaction::find($draw_id);

            if ($draw->status == 0) {

                $user = User::find($draw->user_id);

                if ($process == 1) {

                    $balance = new Balance();
                    $balance->user_id = $user->id;
                    $balance->balance_type = 4;
                    $balance->balance_message = 'Para çekme işleminiz onaylanmıştır.';
                    $balance->amount = $draw->amount;
                    $balance->save();


                } elseif ($process == 2) {

                    $current_balance = $user->balance;
                    $new_balance = $current_balance + $draw->amount;
                    $user->balance = $new_balance;
                    $user->save();

                    $balance = new Balance();
                    $balance->user_id = $user->id;
                    $balance->balance_type = 4;
                    $balance->balance_message = 'Para çekme işleminiz red edilmiştir. Ücret Iadesi : Eski Bakiye: ' . $current_balance . " Yeni Bakiye : " . $new_balance;
                    $balance->amount = $draw->amount;
                    $balance->save();
                }
                $draw->status = $process;
                $draw->save();

                return redirect()->back();
            } else
                return redirect()->back();
        }else
            return redirect('/');


    }


    /**
     * @return array
     */
    public function Getaccounting()
    {
        if($this->permissionCheck()) {

            $getDeposits = TransactionDetails::where('status', 1)->get();
            $getDraws = Transaction::where('status', 1)->get();

            $getBalances = Balance::all();

            $total_deposits = 0;
            $total_draws = 0;
            $total_game_in = 0;
            $total_game_out = 0;

            foreach ($getDeposits as $deposit) {
                $total_deposits += $deposit->amount;
            }

            foreach ($getDraws as $draw) {
                $total_draws += $draw->amount;
            }

            foreach ($getBalances as $balance) {
                if ($balance->balance_type == 1)
                    $total_game_in += ($balance->amount * -1);
                elseif ($balance->balance_type == 2)
                    $total_game_out += $balance->amount;
            }

            return View::make('admin.accounting')
            ->with('totals' ,[
                'total_deposit' => $total_deposits,
                'total_draws' => $total_draws,
                'total_game_in' => $total_game_in,
                'total_game_out' => $total_game_out
            ])
                ->with('user', Auth::getUser());
        }else
            return redirect('/');

    }

    public function insertBalance(Request $request)
    {
        if($this->permissionCheck()) {
            $users = User::all();
            $user = Auth::getUser();

            if ($request->method() == 'POST')
            {
                $user_id = $request->input('user_id');
                $amount = $request->input('amount');

                $user2 = User::find($user_id);

                $current_balance = $user2->balance;
                $new_balance = $current_balance + $amount;
                $user2->balance = $new_balance;
                $user2->save();

                $balance = new Balance();
                $balance->user_id = $user_id;
                $balance->balance_type = 3;
                $balance->balance_message = 'Bakiye Eklenmiştir. Eski Bakiye: ' . $current_balance . " Yeni Bakiye : " . $new_balance;
                $balance->amount = $amount;
                $balance->save();

                if ($balance->id) {
                    return redirect('/menage/balance_add?process=success&message=Bakiye Eklenmiştir.');
                } else
                    return redirect('/menage/balance_add?process=danger&message=İşlem Şuanda Gerçekleştirilemiyor.');
            }

            return View::make('admin.balance_add')
                ->with('user', $user)
                ->with('users', $users);
        } else {
            return redirect('/');
        }
    }

    public function deleteBalance(Request $request)
    {
        if($this->permissionCheck()) {
            $users = User::all();
            $user = Auth::getUser();

            if ($request->method() == 'POST')
            {
                $user_id = $request->input('user_id');
                $amount = $request->input('amount');

                $user2 = User::find($user_id);

                if ($user2->balance < $amount) {
                    return redirect('/menage/balance_remove?process=danger&message=Yetersiz Bakiye. Mevcut Bakiye: '.$user2->balance);
                }

                $current_balance = $user2->balance;
                $new_balance = $current_balance - $amount;
                $user2->balance = $new_balance;
                $user2->save();

                $balance = new Balance();
                $balance->user_id = $user_id;
                $balance->balance_type = 4;
                $balance->balance_message = 'Bakiye Çıkarılmıştır. Eski Bakiye: ' . $current_balance . " Yeni Bakiye : " . $new_balance;
                $balance->amount = $amount;
                $balance->save();

                if ($balance->id) {
                    return redirect('/menage/balance_remove?process=success&message=Bakiye Çıkarılmıştır.');
                } else
                    return redirect('/menage/balance_remove?process=danger&message=İşlem Şuanda Gerçekleştirilemiyor.');
            }

            return View::make('admin.balance_remove')
                ->with('user', $user)
                ->with('users', $users);
        } else {
            return redirect('/');
        }
    }

}