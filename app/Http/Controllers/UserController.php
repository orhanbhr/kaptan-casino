<?php

namespace App\Http\Controllers;


use App\SmsValidate;
use App\UserBank;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Validator;
use Input;

use App\Balance;
use App\User;
use Auth;
use View;


class UserController extends Controller
{


    /**
     * @return mixed
     */
    public function register(Request $request)
    {

        if (Auth::check()) {
            return redirect('/');
        }

        if ($request->isMethod('post')) {


            $required = [
                'username' => 'required',
                'email' => 'required',
                'password' => ['required', 'confirmed', 'min:6', 'max:10'],
                'password_confirmation' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'sex' => 'required',
                'phone' => 'required',
                'location' => 'required',
                'birth_day' => 'required',
                'birth_month' => 'required',
                'birth_year' => 'required',
                '_token' => 'required'
            ];

            $valid_message = [
                'birth_date.required' => 'Lütfen doğum tarihinizi yazınız.',
                'phone.required' => 'Lütfen telefon numaranızı yazınız.',
                'phone.min' => 'Telefon numaranız en az 8 karakter olmalı.',
                'phone.max' => 'Telefon numaranız en fazla 15 karakter olmalı.',
                'country' => 'Lütfen ülke alanını doldurunuz.',
                'address' => 'Lütfen adres alanını doldurunuz.',
                'city' => 'Lütfen şehir alanını doldurunuz.',
            ];

            $valid = Validator::make(Input::all(), $required, $valid_message);
            if (!$valid->fails()) {

                $control = User::where('username', e(Input::get('username')))->get();
                if ($control->count())
                    return Redirect::back()->withErrors(['Bu kullanıcı adına sahip bir kullanıcı mevcut. Lütfen başka bir kullanıcı adı seçiniz.']);

                $control = User::where('email', e(Input::get('email')))->get();
                if ($control->count())
                    return Redirect::back()->withErrors(['Bu email adresi kullanılmaktadır. Lütfen başka bir email adresi veriniz.']);

                $control = User::where('phone', e(Input::get('phone')))->get();
                if ($control->count())
                    return Redirect::back()->withErrors(['Bu telefon numarası kullanılmaktadır. Lütfen başka bir telefon numarası veriniz.']);

                try {


                    $userData = [
                        'username' => e($request->input('username')),
                        'email' => e($request->input('email')),
                        'password' => bcrypt(e($request->input('password'))),
                        'first_name' => e($request->input('first_name')),
                        'last_name' => e($request->input('last_name')),
                        'sex' => e($request->input('sex')),
                        'phone' => e($request->input('phone')),
                        'location' => e($request->input('location')),
                        'birthday' => new \DateTime($request->input('birth_day')."/".$request->input('birth_month')."/".$request->input('birth_year')),
                        'status'   => 2,
                        'created_at' => new \DateTime(),
                        'updated_at' => new \DateTime()
                    ];

                    $createUser = User::insert($userData);

                    if ($createUser) {
                        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                            $xpro = new xProController();
                            $xpro->createAccount();

                            $getUser = User::where('username', $request->input('username'))->get();

                            $sms_validate           = new SmsValidate();
                            $sms_validate->user_id  = $getUser[0]->id;
                            $sms_validate->sms_code = str_random(6);
                            $sms_validate->save();

                            $kurecell = new KurecellController();
                            $kurecell->sendValidate($request->input('phone'),$sms_validate->sms_code);

                            return redirect('/');
                        }

                    } else {
                        return Redirect::back()->withErrors(['Kayıt sırasında bir hata oluştu. Lütfen tekrar deneyiniz.']);
                    }

                } catch (Exception $e) {
                    Log::error($e->getMessage() . ' ' . $e->getLine());
                    $error = $e->getMessage();
                }
            } else {

                $e = "Lütfen tüm alanları doldurunuz.";
                if ($valid->errors()->first()) {
                    $e = $valid->errors()->first();
                }

                return Redirect::back()->withErrors([$e]);
            }
            return Redirect::back()->withErrors(['Bilinmeyen bir hata oluştu.']);
        }else
            return View::make('user.register')
                ->with('user', false);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function login(Request $request)
    {

        if (Auth::check()) {
            return Redirect::to('/');
        }

        $required = [
            'email'     => 'required',
            // 'password'  => ['required', 'min:6', 'max:10'],
            'password' => 'required',
            '_token'    => 'required'
        ];


        $valid = Validator::make(Input::all(), $required);
        if (!$valid->fails()) {

            $email  = $request->input('email');
            $pass   = $request->input('password');


            if (Auth::attempt(['email' => $email, 'password' => $pass]))
                return redirect('/');
            else
                return redirect('/?failed=true')->with('message', 'Login Failed');

        }

        return Response::json(
            [
                'type' => 'error',
                'text' => "Lütfen tüm alanları doldurunuz.",
                'required' => $valid->errors()
            ]
        );
    }


    /**
     * @return mixed
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }


    /**
     * @return bool
     */
    public function getUser()
    {
        if(Auth::check())
        {
            return Auth::getUser();
        }else
            return false;
    }

    /**
     * @param $balance
     */
    public function setUserBalance($balance)
    {
        if($this->getUser()) {
            User::where('id', Auth::getUser()->id)->update(['balance' => $balance]);
            return true;
        }
        else
            return false;
    }

    public function SmsValidate(Request $request){

        if(Auth::check()) {
            if ($request->method() == "POST") {
                $email = $request->input('email');
                $sms_code = $request->input('sms_valid');

                $getUser = User::where('email', $email)->get();
                if ($getUser->count() > 0) {
                    $user_id = $getUser[0]->id;

                    $checkSmsValidate = SmsValidate::where('user_id', $user_id)->where('sms_code', $sms_code)->get();

                    if ($checkSmsValidate->count() > 0) {
                        $updateUser = User::where('id', $user_id)->update(['status' => 1]);

                        if ($updateUser)
                            return redirect('/');
                    } else
                        return redirect()->back()->withErrors(['email' => "Hatalı sms kodu."]);

                } else
                    return redirect()->back()->withErrors(['email' => "Email adresini yanlış girdiniz."]);

            } else
                return View::make('user.sms_validate')
                    ->with('user', Auth::getUser());
        }else
            return redirect('/');
    }

}