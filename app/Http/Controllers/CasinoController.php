<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use DateTime;

use App\Balance;
use App\User;
use Auth;
use View;


class CasinoController extends Controller
{

    /**
     * @return mixed
     */
    public function getGameList()
    {

        if (Auth::check()) {
            $user = Auth::getUser();
        }

        $xpro = new xProController();
        $game_list = $xpro->getGamesListWithLimits();
        if ($game_list['type'] == 'error') {
            return $game_list;
        }

        $liveGames = [];
        foreach ($game_list['data'] as $key => $game) {
            $game['service_id'] = 1;
            $liveGames[$key] = $game;
        }

        return $liveGames;

    }

    /**
     * @param $game_id
     * @return array
     */
    public function createGameUrl($game_id){

        if (Auth::check()) {
            $user = Auth::getUser();
        }


        $xpro = new xProController();

        if($user->xpro_register == 0){
            $xpro->createAccount();
        }


        $token = $xpro->registerToken();
        if($token['type'] == 'error'){
            return Response::json(['type'=>'error','message'=>'Casino bağlantısında problem oluştu. ErrorCode:Token', 'error' => $token]);
        }

        $register_token = $token['data']['register_token'];
        $security_code = $token['data']['security_code'];


        $game_list = $xpro->getGamesListWithLimits(0);
        $game = false;
        foreach($game_list['data'] as $key => $value){
            if($value['game_id'] == $game_id)
            {
                $get_url = $value['url'];
                $get_url = str_replace('{1}','1055',$get_url);
                $get_url = str_replace('{2}',$register_token,$get_url);
                $get_url = str_replace('{3}',$security_code,$get_url);
                $game_list['data'][$key]['url'] = $get_url;
                $value['url'] = $get_url;
                $game = $value;
            }
        }

        if(!$game){
            return ['type'=>'error','message'=>'Oyun bulunuamadı. Lütfen listeden bir oyun seçiniz.'];
        }

        $deposit_result = [];

        $casino_balance = $xpro->getAccountBalance($user->id);
        $game['balance'] = $casino_balance;
        if($casino_balance['type'] == 'success'){
            if(isset($casino_balance['balance']))
            {
                if($casino_balance['balance']<=0){
                    $deposit_result = $xpro->accountDeposit($game);
                    $game['deposit'] = $deposit_result;
                    if($deposit_result['type']=='error'){
                        return Response::json($deposit_result);
                    }
                } else {
                    $result = $xpro->accountWithdrawal('All',false,true);
                    if($result['type'] == 'success') {
                        return ['type' => 'refresh'];
                    }
                }
            }
        }


        User::where('id', $user->id)->update(['casino_last_login' => new DateTime()]);
        return $game;
    }


    /**
     * @return mixed
     */
    public function closeCasino(){

        $xpro = new xProController();

        $result = $xpro->accountWithdrawal('All',false,true);

        return Response::json($result);
    }


    /**
     * @return mixed
     */
    public function getSlots(){

        $xpro = new xProController();
        $game_list_data = $xpro->getExternalGamesList();
        $game_list = [];

        foreach($game_list_data as $game){
            $game_id = (int) $game->gameID;
            $game_type = (string) $game->typeID;

            $cat = 'other_games';

            if($game_type == 'Slots'){ $cat = 'classic_slots'; }
            if($game_type == 'Table'){ $cat = 'table_games'; }
            if($game_type == 'Video Poker'){ $cat = 'video_poker'; }
            if($game_type == 'Pyramid Poker'){ $cat = 'poker'; }
            if($game_type == 'Multihand Poker'){ $cat = 'poker'; }
            if($game_type == 'Soft Games'){ $cat = 'soft_games'; }

            $game_name = (string) $game->gameName;
            if(strpos($game_name,'Blackjack')){
                $cat = 'table_games';
            }

            if(strpos($game_name,'Roulette')){
                $cat = 'table_games';
            }

            $imageUrl = str_replace('http://','https://', (string)$game->imageUrl);

            if(strpos($game->imageUrl, 'http') === false && $game->imageUrl != "")
                $imageUrl = "https://lcpp.xprogaming.com/Resources/TopGame".$game->imageUrl;

            $game_list[$cat][] = [
                'server_id' => 1,
                'game_id' => (int)$game->gameID,
                'type_id' => (int)$game->typeID,
                'provider' => (string)$game->gameProvider,
                'game_name' => (string)$game->gameName,
                'image_url' => (string)$imageUrl,
                'languages' => explode(',',$game->languages),
                'maxWin' => (int)$game->maxWin,
                'lines_count' => (int)$game->linesCount,
                'game_type' => $cat
            ];
        }


        return $game_list;
    }


    /**
     * @param $game_id
     * @param $provider
     * @return mixed
     */
    public function getSlotUrl($game_id,$provider){

        if (Auth::check()) {
            $user = Auth::getUser();
        }


        if(!$game_id){
            return Response::json(['type'=>'error','message'=>'Lütfen oyun seçiniz.']);
        }


        $server_id  = 1;

        $xpro = new xProController();


        $login_game = [
            'game_name' => 'Slot',
        ];


        $casino_balance = $xpro->getAccountBalance();
        if($casino_balance['type'] == 'success'){
            if(isset($casino_balance['balance']))
            {
                if((int)$casino_balance['balance'] == 0){
                    $deposit_result = $xpro->accountDeposit($login_game);
                    if($deposit_result['type']=='error'){
                        return Response::json($deposit_result);
                    }
                }else {
                    $result = $xpro->accountWithdrawal('All');
                    if($result['type'] == 'success') {
                        return ['type' => 'refresh'];
                    }
                }
            }
        }

        if($server_id == 1) {
            $game_list_data = $xpro->getExternalGameURL($game_id, $provider);
        }

        return $game_list_data;

    }

}