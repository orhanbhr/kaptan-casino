<?php

namespace App\Http\Controllers;


use App\Settings;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Validator;
use Input;

use App\Balance;
use App\User;
use App\UserBank;
use App\BalanceLog;
use App\Transaction;
use App\TransactionDetails;
use Auth;
use View;
use Illuminate\Support\Facades\Hash;


class WebController extends Controller
{

    /**
     * @return bool
     */
    public function userCheck()
    {
        if (Auth::check()) {
            $user = Auth::getUser();
            if($user->status == 1)
                return true;
            else
                return false;
        }else
            return false;
    }

    /**
     * @return mixed
     */
    public function index()
    {
       $user = false;
       if ($this->userCheck()) {

           $user = Auth::getUser();

           if($user->xpro_register == 1){
               $casino = new CasinoController();
               $casino->closeCasino();

           }

       }

        if(Auth::check())
        {
            $user = Auth::getUser();
            if($user->status == 2)
                return redirect('/user/sms_validate');
        }

       return View::make('index')
           ->with('user', $user);
    }

    /**
     * @return mixed
     */
    public function getSlotGames()
    {

        $user = false;

        if ($this->userCheck()) {

            $user = Auth::getUser();

            if($user->xpro_register == 1){
                $casino = new CasinoController();
                $casino->closeCasino();

            }

        }

        if(Auth::check())
            $user = Auth::getUser();


        $casino = new CasinoController();

        $slotGames = $casino->getSlots();

        return View::make('slots')
            ->with('games', $slotGames)
            ->with('user', $user);

    }

    /**
     * @param $game_id
     * @param $provider
     * @return mixed
     */
    public function getSlotGame($game_id, $provider)
    {

        if($this->userCheck())
            $user = Auth::getUser();
        else
            return redirect('/');

        $casino = new CasinoController();

        $slotGame = $casino->getSlotUrl($game_id, $provider);

        if(isset($slotGame['type']))
            if($slotGame['type'] == 'refresh')
                return redirect('/slot/game/'.$game_id.'/'.$provider);

        return View::make('game')
            ->with('game_url', $slotGame['url'])
            ->with('user', $user);
    }

    /**
     * @return mixed
     */
    public function getLiveGames()
    {
        $user = false;

        if ($this->userCheck()) {

            $user = Auth::getUser();

            if($user->xpro_register == 1){
                $casino = new CasinoController();
                $casino->closeCasino();

            }

        }

        if(Auth::check())
            $user = Auth::getUser();


        $casino = new CasinoController();

        $liveGames = $casino->getGameList();


        return View::make('casino')
            ->with('games', $liveGames)
            ->with('user', $user);
    }

    /**
     * @param $game_id
     * @return Redirect|string
     */
    public function getLiveGame($game_id)
    {

        if($this->userCheck())
            $user = Auth::getUser();
        else
            return redirect('/');


        $casino = new CasinoController();

        $slotGame = $casino->createGameUrl($game_id);



        if(isset($slotGame['type']))
            if($slotGame['type'] == 'refresh')
                return redirect('/casino/game/'.$game_id);


        return View::make('game')
            ->with('game_url', $slotGame['url'])
            ->with('user', $user);
    }

    /**
     * @return string
     */
    public function getBonus()
    {
        if(Auth::check())
            $user = Auth::getUser();
        else
            $user = false;

        return View::make('bonus')
            ->with('user', $user);
    }

    /**
     * @return string
     */
    public function getHelpDeposits()
    {
        if(Auth::check())
            $user = Auth::getUser();
        else
            $user = false;

        return View::make('help/deposits')
            ->with('user', $user);
    }

    /**
     * @return string
     */
    public function getHelpWithdraw()
    {
        if(Auth::check())
            $user = Auth::getUser();
        else
            $user = false;

        return View::make('help/withdraw')
            ->with('user', $user);
    }

    /**
     * @return string
     */
    public function getHelpTermOfUse()
    {
        if(Auth::check())
            $user = Auth::getUser();
        else
            $user = false;

        return View::make('help/termsofuse')
            ->with('user', $user);
    }

    /**
     * @return string
     */
    public function getHelpAffilate()
    {
        if(Auth::check())
            $user = Auth::getUser();
        else
            $user = false;

        return View::make('help/affilate')
            ->with('user', $user);
    }

    /**
     * @return string
     */
    public function getUserAccountDetail()
    {
        if($this->userCheck())
            $user = Auth::getUser();
        else
            return redirect('/');

        return View::make('user/account_detail')
            ->with('user', $user);
    }

    /**
     * @return Redirect
     */
    public function getUserBankAccounts()
    {
        if($this->userCheck()) {
            $user = Auth::getUser();



            $userBanks = UserBank::where('user_id', $user->id)->get();

            return View::make('user.bank_accounts')
                ->with('user', $user)
                ->with('banks', $userBanks);
        }else
            return redirect('/');
    }

    /**
     * @return Redirect
     */
    public function getUserFinance()
    {
        if($this->userCheck()) {
            $user = Auth::getUser();

            $start_date_day   = false;
            $start_date_month = false;
            $start_date_year  = false;
            $end_date_day     = false;
            $end_date_month   = false;
            $end_date_year    = false;
            $process          = false;

            if(isset($_REQUEST['start_date_day']))
                $start_date_day = $_REQUEST['start_date_day'];
            if(isset($_REQUEST['start_date_month']))
                $start_date_month = $_REQUEST['start_date_month'];
            if(isset($_REQUEST['start_date_year']))
                $start_date_year = $_REQUEST['start_date_year'];
            if(isset($_REQUEST['end_date_day']))
                $end_date_day   = $_REQUEST['end_date_day'];
            if(isset($_REQUEST['end_date_month']))
                $end_date_month   = $_REQUEST['end_date_month'];
            if(isset($_REQUEST['end_date_year']))
                $end_date_year   = $_REQUEST['end_date_year'];
            if(isset($_REQUEST['process']))
                $process   = $_REQUEST['process'];

            $start_date = $start_date_year.'-'.$start_date_month.'-'.$start_date_day;
            $end_date   = $end_date_year.'-'.$end_date_month.'-'.$end_date_day;


            $userFinances = BalanceLog::where('user_id', $user->id)->paginate(25);

            if($start_date && $end_date && !empty($process))
                $userFinances = BalanceLog::where('user_id', $user->id)
                    ->where('balance_type', $process)
                    ->where('created_at', '>=', $start_date)
                    ->where('created_at', '<=',$end_date)->paginate(25);

            if($start_date && $end_date && empty($process))
                $userFinances = BalanceLog::where('user_id', $user->id)
                    ->where('created_at', '>=', $start_date)
                    ->where('created_at', '<=',$end_date)->paginate(25);

            return View::make('user.finance')
                ->with('user', $user)
                ->with('finances', $userFinances);
        }else
            return redirect('/');
    }

    /**
     * @return Redirect
     */
    public function getUserChangePassword()
    {
        if($this->userCheck()) {
            $user = Auth::getUser();

            return View::make('user.change_password')
                ->with('user', $user);
        }else
            return redirect('/');
    }

    /**
     * @return mixed
     */
    public function getUserChangePasswordUpdate()
    {
        if(!Auth::check()){
            return Redirect::to('login');
        }

        $user = Auth::getUser();

        $old_password 	= $_REQUEST['old_password'];
        $password 		= $_REQUEST['password'];

        if (Hash::check($old_password, $user->password))
        {
            $user->password = bcrypt($password);
            $user->save();

            return Redirect::back()->withErrors(['Şifreniz Güncellenmiştir.']);
        }else
        {
            return Redirect::back()->withErrors(['Eski şifrenizi yanlış girdiniz.']);
        }
    }

    /**
     * @return Redirect
     */
    public function getUserBonus()
    {
        if($this->userCheck()) {
            $user = Auth::getUser();

            return View::make('user.bonus')
                ->with('user', $user);
        }else
            return redirect('/');
    }

    /**
     * @return Redirect
     */
    public function getUserBankAdd(Request $request)
    {
        if($this->userCheck()) {
            $user = Auth::getUser();

            if ($request->method() == 'POST')
            {
                $user_id      = $user->id;
                $bank_name    = $request->input('bank_name');
                $account_name = $request->input('account_name');
                $iban         = $request->input('iban');

                $user_bank = new UserBank();

                $user_bank->user_id       = $user_id;
                $user_bank->bank_name     = $bank_name;
                $user_bank->account_name  = $account_name;
                $user_bank->iban          = $iban;
                $user_bank->save();

                if ($user_bank->id) {
                    return redirect('/user/bank-add?process=success');
                } else
                    return redirect('/user/bank-add?process=error&message=İşlem Şuanda Gerçekleştirilemiyor.');
            }

            return View::make('user.bank_add')
                ->with('user', $user);
        }else
            return redirect('/');
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function getUserWithDraw(Request $request)
    {
        if($this->userCheck()) {
            $user = Auth::getUser();

            if($request->method() == 'POST')
            {
                $user_id        = $user->id;
                $draw_amount    = $request->input('draw_amount');
                $bank           = $request->input('bank');

                if($user->balance < $draw_amount)
                    return redirect('/user/withdraw?process=error&message=Bu işlem için yeterli bakiyeniz bulunmamaktadır.');

                $transaction = new Transaction();

                $transaction->user_id = $user_id;
                $transaction->transaction_type = 2;
                $transaction->amount = $draw_amount;
                $transaction->user_bank = $bank;
                $transaction->save();

                if($transaction->id) {
                    $userController = new UserController();
                    $user_balance = ($user->balance - $draw_amount);
                    if($userController->setUserBalance($user_balance))
                        return redirect('/user/withdraw?process=success');
                    else
                        return redirect('/user/withdraw?process=error&message=İşlem Şuanda Gerçekleştirilemiyor.');
                }
                else
                    return redirect('/user/withdraw?process=error&message=İşlem Şuanda Gerçekleştirilemiyor.');

            }

            $userBanks = UserBank::where('user_id', $user->id)->get();


            return View::make('user.withdraw')
                ->with('user', $user)
                ->with('banks', $userBanks);
        }else
            return redirect('/');
    }

    /**
     * @param Request $request
     * @return Redirect
     */
    public function getUserBankDeposits(Request $request)
    {
        if($this->userCheck()) {
            $user = Auth::getUser();

            if ($request->method() == 'POST')
            {
                $user_id          = $user->id;
                $name             = $request->input('name');
                $tc_number        = $request->input('tc_number');
                $bank_name        = $request->input('bank_name');
                $referance_number = $request->input('referance_number');
                $amount           = $request->input('amount');

                if($amount < '50')
                    return redirect('/user/bank_deposits?process=error&message=Bu işlemde minimum para yatırma limiti 50 TLdir.');

                $transaction_details = new TransactionDetails();

                $transaction_details->user_id          = $user_id;
                $transaction_details->name             = $name;
                $transaction_details->transaction_id   = 1;
                $transaction_details->transfer_method  = 1;
                $transaction_details->tc_number        = $tc_number;
                $transaction_details->bank_name        = $bank_name;
                $transaction_details->referance_number = $referance_number;
                $transaction_details->amount           = $amount;
                $transaction_details->save();

                if ($transaction_details->id) {
                    return redirect('/user/bank_deposits?process=success');
                } else {
                    return redirect('/user/bank_deposits?process=error&message=İşlem Şuanda Gerçekleştirilemiyor.');
                }
            }

            return View::make('user.bank_deposits')
                ->with('user', $user);
        }else
            return redirect('/');
    }

    public function getUserCepBankDeposits(Request $request)
    {
        if($this->userCheck()) {
            $user = Auth::getUser();

            if ($request->method() == 'POST')
            {
                $user_id          = $user->id;
                $tc_number        = $request->input('tc_number');
                $bank_name        = $request->input('bank_name');
                $referance_number = $request->input('referance_number');
                $amount           = $request->input('amount');
                $sender_phone     = $request->input('sender_phone');
                $receiver_phone   = $request->input('receiver_phone');

                if($amount < '50')
                    return redirect('/user/cepbank_deposits?process=error&message=Bu işlemde minimum para yatırma limiti 50 TLdir.');

                $transaction_details = new TransactionDetails();

                $transaction_details->user_id          = $user_id;
                $transaction_details->bank_name        = $bank_name;
                $transaction_details->transaction_id   = 1;
                $transaction_details->transfer_method  = 2;
                $transaction_details->tc_number        = $tc_number;
                $transaction_details->referance_number = $referance_number;
                $transaction_details->amount           = $amount;
                $transaction_details->sender_phone     = $sender_phone;
                $transaction_details->receiver_phone   = $receiver_phone;
                $transaction_details->save();

                if ($transaction_details->id) {
                    return redirect('/user/cepbank_deposits?process=success');
                } else {
                    return redirect('/user/cepbank_deposits?process=error&message=İşlem Şuanda Gerçekleştirilemiyor.');
                }
            }

            return View::make('user.cepbank_deposits')
                ->with('user', $user);
        }else
            return redirect('/');
    }


    /**
     * @return mixed
     */
    public static function getTitle()
    {
        $getSettings = Settings::first()->get();

        return $getSettings[0]->site_title;
    }

}