<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use DB;

use App\Balance;
use App\User;
use Auth;
use View;
use Illuminate\Support\Facades\Hash;


class xProController extends Controller
{

    public $operatorID = 197;
    public $privateKey = 'LCPP_197';
    //public $url = 'https://api.xpgnet.com/Services/Externalapi.svc/';
    public $url = 'http://lcpp.xprogaming.com/API/Services/ExternalApi.svc/';


    /**
     * @param $method
     * @param $params
     * @return mixed
     */
    public function requestxPro($method, $params)
    {

        $url = $this->url.$method;

        $md5 = $this->privateKey.http_build_query($params);
        $accessPassword = md5($md5);

        //$params['accessPassword'] = strtoupper($accessPassword);

        $params = array_merge(['accessPassword'=>strtoupper($accessPassword)],$params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $curl_text = curl_exec($ch);
        curl_close($ch);

        return $curl_text;
    }


    /**
     * @return array
     */
    public function createAccount(){

        if(Auth::check()){
            //return Response::json(['type'=>'error','message'=>'Lütfen giriş yapınız.']);
            $user = Auth::getUser();
        }

        if($user->xpro_register == 1){
            return ['type'=>'success','message'=>'Üye kayıtlı'];
        }


        $method = 'createAccount';

        $username = $user->username;
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $accountID = $user->id;
        $isExternalWallet = null;
        $Nickname = $user->username;
        $groupLimitId = null;

        $params = [
            'operatorId' => $this->operatorID,
            'username' => $username,
            'userPassword' => "85233145b0fcffa62087a13e487328ed",
            'firstName' => $firstName,
            'lastName' => $lastName,
            'accountID' => '00'.$accountID,
            'isExternalWallet' => $isExternalWallet,
            'Nickname' => $Nickname,
            'groupLimitId' => $groupLimitId
        ];


        $returnRequest = $this->requestxPro($method, $params);


        $returnRequest = str_replace('xmlns="apiResultData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);


        if($xml->errorCode != 0){
            $message = 'Casino, sisteminde hata bulundu. İşlem tamamlanamıyor.'.$xml->errorCode;

            if($xml->errorCode == 3){ $message = $username.' adında bir kullanıcı mevcut. Lütfen farklı bir kullanıcı adı belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 4){ $message = $username.' adında bir nick mevcut. Lütfen farklı bir nick belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 5){ $message = 'Eksik parametrelerden dolayı casino bağlanılamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 6){ $message = 'Casino hizmetimiz geçici olarak servis dışıdır. Hata Kodu:'.$xml->errorCode; }

            return ['type'=>'error','message'=>$message, 'xml' => $xml];
        }
        else{
            User::where('id','=',$user->id)->update(['xpro_register'=>1]);
            return ['type'=>'success','message'=>'Kullanıcı oluşturuldu.'];
        }
    }


    /**
     * @param $game
     * @return array
     */
    public function accountDeposit($game){


        if(Auth::check()){
            $user = Auth::getUser();
        }

        $method = 'accountDeposit';
        $username = $user->username;
        $balance = 0-$user->balance;

        $balance_message = $game['game_name'].' oyununa aktarım.';
        $balance_type = 1;

        $balance_data = [
            'user_id'                   => $user->id,
            'balance_type'              => (int)$balance_type,
            'balance_message'           => (string)$balance_message,
            'amount'                    => $balance,
            'created_at'                => new \DateTime()
        ];

        $transaction_id = Balance::insertGetId($balance_data);
        if(!$transaction_id){
            return ['type'=>'error','message'=>'Hesap Ücret Aktarımı Sırasında Bir Hata Oluştu.'];
        }


        $params = [
            'operatorId' => $this->operatorID,
            'username' => $username,
            'amount' => floatval($user->balance),
            'transactionID' => 'kaptancasino-'.$transaction_id.'-'.time()
        ];



        $returnRequest = $this->requestxPro($method, $params);


        $returnRequest = str_replace('xmlns="apiTransactionData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);


        # 10 - PAra yoksa verir. Parası yoksa da girsin...
        if ($xml->errorCode != 0 AND $xml->errorCode != 10) {
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hata Kodu: BLNC-' . $xml->errorCode;

            if ($xml->errorCode == 2) {
                $message = $username . ' adında bir kullanıcı bulunamadı. Hata Kodu:' . $xml->errorCode;
            } else if ($xml->errorCode == 15) {
                $message = 'Güncelleme yaparken bir hata oluştu. Hata Kodu:' . $xml->errorCode;
            } else if ($xml->errorCode == 5) {
                $message = 'Eksik parametrelerden dolayı casino bağlanılamadı. Hata Kodu:' . json_encode($xml);
            } else if ($xml->errorCode == 6) {
                $message = 'Casino hizmetimiz geçici olarak servis dışıdır. Hata Kodu:' . $xml->errorCode;
            } else if ($xml->errorCode == 8) {
                $message = 'Hesap casino tarafında banlandığından dolayı ücret aktarımı yapılamadı. Hata Kodu:' . $xml->errorCode;
            } else if ($xml->errorCode == 9) {
                $message = 'Transaction ID ile aktarım mevcut olduğundan, işleminiz gerçekleştirilemedi. Lütfen tekrar deneyiniz. Hata Kodu:' . $xml->errorCode;
            }
            # Hataya düşerse balans logunu sil.
            Balance::where('id', '=', $transaction_id)->delete();
            return ['type' => 'error', 'message' => $message, 'xml' => $xml];
        } else {
            # Parası varsa error 0 ile girer, yoksa error 10 ile girer ve 0 tl ile ancak izler...
            if ($xml->errorCode == 0 OR $xml->errorCode == 10) {
                // Tüm Parası transfer edilmeli. Paranın xProGame casinosunda olduğunu belirtmek için money_in_casino ID 1 olmalı.
                User::where('id', '=', $user->id)->update(['balance' => 0, 'casino_last_login'=>new \DateTime()]);
            }
            return ['type' => 'success', 'message' => 'Ücret, casinoya aktarıldı.'];
        }


    }


    /**
     * @return array
     */
    public function getAccountBalance(){

        if(Auth::check()){
            $user = Auth::getUser();
        }

        $method = 'getAccountBalance';

        $username = $user->username;

        $params = [
            'operatorId' => $this->operatorID,
            'username' => $username,
        ];


        $returnRequest = $this->requestxPro($method, $params);


        $returnRequest = str_replace('xmlns="apiAccountBalanceData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);


        if($xml->errorCode != 0){
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hesap kurulamadı. Hata Kodu:'.$xml->errorCode;

            if($xml->errorCode == 2){ $message = $username.' adında bir kullanıcı bulunamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 15){ $message = 'Güncelleme yaparken bir hata oluştu. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 5){ $message = 'Eksik parametrelerden dolayı casino bağlanılamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 6){ $message = 'Casino hizmetimiz geçici olarak servis dışıdır. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 8){ $message = 'Hesap casino tarafında banlandığından dolayı ücret aktarımı yapılamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 9){ $message = 'Transaction ID ile aktarım mevcut olduğundan, işleminiz gerçekleştirilemedi. Lütfen tekrar deneyiniz. Hata Kodu:'.$xml->errorCode; }

            return ['type'=>'error','message'=>$message, 'xml' => $xml];
        }
        else{
            return ['type'=>'success','message'=>'Casino hesabınızda '.$xml->balance.' TL mevcuttur.','balance'=>floatval($xml->balance)];
        }

    }

    /**
     * @param int $debug
     * @return bool|float|\SimpleXMLElement
     */
    public function getUserBalance($debug = 0){

        if(Auth::check()){
            $user = Auth::getUser();
        }


        $method     = 'getAccountBalance';
        $username   = $user->username;
        $params     = [
            'operatorId' => $this->operatorID,
            'username' => $username,
        ];


        $returnRequest = $this->requestxPro($method, $params);

        $responseText = str_replace('xmlns="apiAccountBalanceData"','',$returnRequest);
        $xml = simplexml_load_string($responseText);

        if($debug)
            return $xml;

        if(((int) $xml->errorCode) == 0){
            return floatval($xml->balance);
        }

        return false;
    }

    /**
     * @return array
     */
    public function getAccountBalances(){

        $method = 'getAccountBalances';

        $username = "-1";


        $time =(time() * 10000000) + 621355968000000000 ;
        $params = [
            'operatorId' => $this->operatorID,
            'usernames' => $username,
            'date' => $time,
        ];


        $returnRequest = $this->requestxPro($method, $params);


        $returnRequest = str_replace('xmlns="apiAccountBalancesData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);


        if($xml->errorCode != 0){
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hesap kurulamadı. Hata Kodu:'.$xml->errorCode;

            if($xml->errorCode == 2){ $message = $username.' adında bir kullanıcı bulunamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 15){ $message = 'Güncelleme yaparken bir hata oluştu. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 5){ $message = 'Eksik parametrelerden dolayı casino bağlanılamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 6){ $message = 'Casino hizmetimiz geçici olarak servis dışıdır. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 8){ $message = 'Hesap casino tarafında banlandığından dolayı ücret aktarımı yapılamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 9){ $message = 'Transaction ID ile aktarım mevcut olduğundan, işleminiz gerçekleştirilemedi. Lütfen tekrar deneyiniz. Hata Kodu:'.$xml->errorCode; }

            return ['type'=>'error','message'=>$message, 'xml' => $xml];
        }
        else{
            $balances_xml = gzdecode(base64_decode($xml->CompressedData));
            $balancesData = simplexml_load_string($balances_xml);
            $balances = [];
            foreach($balancesData as $balance){
                $balances[] = ['username'=>(string)$balance->nickname,'balance'=>floatval($balance->balance)];
            }
            return ['type'=>'success','message'=>'Casino hesabınızda '.$xml->balance.' TL mevcuttur.','balances'=>$balances];
        }

    }

    /**
     * @param string $amount
     * @return array
     */
    public function accountWithdrawal($amount='All'){

        if(Auth::check()){
            $user = Auth::getUser();
        }

        $method = 'accountWithdrawal';


        // Casio Session Clear
        $casinoData = $this->getAccountBalance();

        $username = $user->username;
        $amountInCasinoData = [];
        if($amount == 'All'){
            $amountInCasinoData = $this->getAccountBalance($user->id);
            $amount = $amountInCasinoData["balance"];
        }

        $transaction_data = [
            'user_id' => $user->id,
            'balance_type' => '2',
            'balance_message' => 'Casinodan hesaba aktarım.',
            'amount' => $amount,
            'created_at' => new \DateTime()
        ];

        $transaction_id = Balance::insertGetId($transaction_data);

        if(!$transaction_id){
            return ['type'=>'error','message'=>'Transaction oluşmadığından dolayı işleminizi gerçekleştiremedik. Lütfen tekrar deneyiniz.'];
        }

        $params = [
            'operatorId' => $this->operatorID,
            'username' => $username,
            'amount' => floatval($amount),
            'transactionID' => 'kaptancasino-'.$transaction_id.'-'.time(),
        ];



        $returnRequest = $this->requestxPro($method, $params);


        $returnRequest = str_replace('xmlns="apiTransactionData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);


        if($xml->errorCode != 0){
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hata Kodu:'.$xml->errorCode;

            if($xml->errorCode == 3){ $message = $username.' adında bir kullanıcı mevcut. Lütfen farklı bir kullanıcı adı belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 1){ $message = 'Casinoda belirtilen bakiye kadar ücret yok. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 2){ $message = $username.' adında bir kullanıcı yok. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 4){ $message = $username.' adında bir nick mevcut. Lütfen farklı bir nick belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 5){ $message = 'Eksik parametrelerden dolayı casino bağlanılamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 6){ $message = 'Casino hizmetimiz geçici olarak servis dışıdır. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 8){ $message = 'Kullanıcı banlandığından dolayı ücret çekilemez. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 9){ $message = 'Daha önce aynı işlem numarası ile para çekilmiş. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 10){ $message = 'Hatalı miktar. Lütfen geçerli bir miktar giriniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 16){ $message = 'Belirtilen Email adresi kullanılıyor. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 29){ $message = 'Şifre çok kısa ve güvensiz. Lütfen daha uzun ve güvenli bir şifre belirtiniz. Hata Kodu:'.$xml->errorCode; }
            Balance::where('id','=',$transaction_id)->delete();
            return ['type'=>'error','message'=>$message, 'xml' => $xml, 'balance' => $amount, 'casino' => $casinoData];
        }else{
                $new_price = $user->balance + $amount;
                $updated = [
                    'balance'     => $new_price,
                ];
                $updateData = User::where('id', '=', $user->id)->update($updated);

            if($updateData)
                return ['type'=>'success','message'=>'Çıkış işleminiz tamamlandı.', 'response' => $amountInCasinoData];
        }
    }

    /**
     * @param bool|false $userid
     * @return array
     */
    public function registerToken(){

        if(Auth::check()){
            $user = Auth::getUser();
        }


        $method = 'registerToken';

        $username = $user->username;


        $params = [
            'operatorId' => $this->operatorID,
            'username' => $username,
            'props' => 'ExternalSessionID:'.\Session::getId(),
        ];


        $returnRequest = $this->requestxPro($method, $params);


        $returnRequest = str_replace('xmlns="apiResultData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);

        if($xml->errorCode != 0){
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hata Kodu:'.$xml->errorCode;

            if($xml->errorCode == 3){ $message = $username.' adında bir kullanıcı mevcut. Lütfen farklı bir kullanıcı adı belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 1){ $message = 'Casinoda belirtilen bakiye kadar ücret yok. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 2){ $message = $username.' adında bir kullanıcı yok. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 4){ $message = $username.' adında bir nick mevcut. Lütfen farklı bir nick belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 5){ $message = 'Eksik parametrelerden dolayı casino bağlanılamadı. Hata Kodu:'.$xml->errorCode.' '.$xml->description; }
            else if($xml->errorCode == 6){ $message = 'Casino hizmetimiz geçici olarak servis dışıdır. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 8){ $message = 'Kullanıcı banlandığından dolayı ücret çekilemez. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 9){ $message = 'Daha önce aynı işlem numarası ile para çekilmiş. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 10){ $message = 'Hatalı miktar. Lütfen geçerli bir miktar giriniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 16){ $message = 'Belirtilen Email adresi kullanılıyor. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 29){ $message = 'Şifre çok kısa ve güvensiz. Lütfen daha uzun ve güvenli bir şifre belirtiniz. Hata Kodu:'.$xml->errorCode; }
            return ['type'=>'error','message'=>$message, 'xml' => $xml];
        }
        else{
            $register_token = (string)$xml->description;
            $security_code = strtoupper(md5($this->privateKey.$this->operatorID.$register_token));
            return ['type'=>'success','message'=>'Register Token Alındı.','data'=>['register_token'=>$register_token,'security_code'=>$security_code]];
        }
    }

    /**
     * @param $game_type
     * @return array
     */
    public function getGamesListWithLimits(){

        if(!Auth::check()){
            //return Response::json(['type'=>'error','message'=>'Lütfen giriş yapınız.']);
            $user = User::find(1);
        }else {
            $user = Auth::getUser();
        }

        if($user->xpro_register == 0){
            $this->createAccount();
        }


        $method = 'getGamesListWithLimits';

        $username = $user->username;

        $params = [
            'operatorId' => $this->operatorID,
            'username' => $username,
            'gameType' => 0,
            'onlineOnly' =>1,
            'includeInOutLimits' => 1,
            'IncludeRouletteLastResults' => 1,
        ];


        $returnRequest = $this->requestxPro($method, $params);

        $returnRequest = str_replace('xmlns="apiGamesLimitsListData"','',$returnRequest);

        $xml = simplexml_load_string($returnRequest);


        if($xml->errorCode != 0){
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hata Kodu:'.$xml->errorCode;

            if($xml->errorCode == 3){ $message = $username.' adında bir kullanıcı mevcut. Lütfen farklı bir kullanıcı adı belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 1){ $message = 'Casinoda belirtilen bakiye kadar ücret yok. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 2){ $message = $username.' adında bir kullanıcı yok. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 4){ $message = $username.' adında bir nick mevcut. Lütfen farklı bir nick belirtiniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 5){ $message = 'Eksik parametrelerden dolayı casino bağlanılamadı. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 6){ $message = 'Casino hizmetimiz geçici olarak servis dışıdır. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 8){ $message = 'Kullanıcı banlandığından dolayı ücret çekilemez. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 9){ $message = 'Daha önce aynı işlem numarası ile para çekilmiş. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 10){ $message = 'Hatalı miktar. Lütfen geçerli bir miktar giriniz. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 16){ $message = 'Belirtilen Email adresi kullanılıyor. Hata Kodu:'.$xml->errorCode; }
            else if($xml->errorCode == 29){ $message = 'Şifre çok kısa ve güvensiz. Lütfen daha uzun ve güvenli bir şifre belirtiniz. Hata Kodu:'.$xml->errorCode; }
            return ['type'=>'error','message'=>$message, 'xml' => $xml];
        }
        else{
            $data = [];
            foreach($xml->gamesList->game as $game){
                $url = $game->connectionUrl;
                $url = str_replace('http://livegames.xprogaming.com/GeneralGame.aspx',
                    'https://livegameslocal.xpgnet.com/GeneralGame.aspx', $url);

                $data[] = [
                    'limits' => [
                        'limit_id' => (int)$game->limitSetList->limitSet->limitSetID,
                        'min_bet' => floatval($game->limitSetList->limitSet->minBet),
                        'max_bet' => floatval($game->limitSetList->limitSet->maxBet),
                        'min_inside_bet' => floatval($game->limitSetList->limitSet->minInsideBet),
                        'max_inside_bet' => floatval($game->limitSetList->limitSet->maxInsideBet),
                        'min_outside_bet' => floatval($game->limitSetList->limitSet->minOutsideBet),
                        'max_outside_bet' => floatval($game->limitSetList->limitSet->maxOutsideBet),
                    ],
                    'last_numbers' => $game->rouletteLastResults->item,
                    'game_id' => (int)$game->gameID,
                    'gameType' => (int)$game->gameType,
                    'game_type' => null,
                    'game_name' => (string)$game->gameName,
                    'dealer_name' => (string)$game->dealerName,
                    'dealer_img' => str_replace('newresources.xprogaming.com','livegameslocal.xpgnet.com',str_replace('http://','https://',(string)$game->dealerImageUrl)),
                    'is_open' => (int) $game->isOpen,
                    'url' => str_replace('livegames.xpgnet.com','livegameslocal.xpgnet.com',str_replace('http://','https://',(string) $url)),
                    'open_hour' => (string) $game->openHour,
                    'close_hour' => (string) $game->closeHour,
                    'max_player' => (int) $game->PlayersNumber,
                    'now_online' => (int) $game->PlayersNumberInGame,
                ];
            }

            return ['type'=>'success','message'=>'Oyun Listesi Çekildi.','data'=>$data];
        }
    }

    /**
     * @return array
     */
    public function getExternalGamesList(){


        $method = 'getExternalGamesList';


        $params = [
            'operatorId' => $this->operatorID,
            'platformType' => 'All',
            'isNewVersion' => 1
        ];


        $returnRequest = $this->requestxPro($method, $params);

        $returnRequest = str_replace('xmlns="apiExternalGamesListData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);


        if($xml->errorCode != 0){
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hata Kodu:'.$xml->errorCode;

            return ['type'=>'error','message'=>$message, 'xml' => $xml];
        }

        else{
            $games = [];
            foreach($xml->GamesList->game as $game){
                if($game->gameProvider != "BtoBet") {
                    //$game['servis'] = $game->gameProvider;
                    $games[] = $game;
                }

            }
            return $games;
        }
    }

    /**
     * @param $game_id
     * @param $provider
     * @return array
     */
    public function getExternalGameURL($game_id, $provider){

        if(Auth::check()){
            //return Response::json(['type'=>'error','message'=>'Lütfen giriş yapınız.']);
            $user = Auth::getUser();
        }

        if($user->xpro_register == 0){
            $this->createAccount();
        }


        $method = 'getExternalGameURL';

        $ServiceProvider = $provider;


        $params = [
            'operatorId' => $this->operatorID,
            'username' => $user->username,
            'gameId' => $game_id,
            'gameProvider' => $ServiceProvider
        ];


        $returnRequest = $this->requestxPro($method, $params);

        $returnRequest = str_replace('xmlns="apiExternalGameLinkData"','',$returnRequest);
        $xml = simplexml_load_string($returnRequest);



        if($xml->errorCode != 0){
            $message = 'Hata. Casino bağlanırken bir hata oluştu. Hata Kodu:'.$xml->errorCode;
            return ['type'=>'error','message'=>$message, 'xml' => $xml];
        }
        else{
            $last_url = str_replace('http://', 'https://', urldecode((string)$xml->gameUrl));
            //livegameslocal.xpgnet.com
            $returnUrl = explode('returnUrl=', $last_url);
            if(count($returnUrl) > 1)
                $last_url = $returnUrl[1];
            return ['type'=>'success','message'=>'URL Oluşturuldu.','url'=>$last_url];
        }
    }


}

