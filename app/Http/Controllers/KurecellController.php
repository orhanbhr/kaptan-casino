<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use DateTime;

use App\Balance;
use App\User;
use Auth;
use View;


class KurecellController extends Controller
{

    /**
     * @param $url
     * @param $requestData
     * @return mixed
     */
    public function send_sms ($url, $requestData) {

        $ch=curl_init();
        $veri = http_build_query($requestData);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1) ;
        curl_setopt($ch, CURLOPT_POSTFIELDS,$veri);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @param $number
     * @return mixed
     */
    public function testKureCell($number)
    {
        $numbers = array($number);
        $message = 'Api Test';
        $title   = 'KAPTANCASNO';

        $data = array(
            'apiNo'         =>  '1',
            'user'          =>  '5324645883',
            'pass'          =>  'kral00',
            'mesaj'         =>  $message,
            'numaralar'     =>  $numbers,
            'baslik'        =>  $title,
        );

        $send_message = $this->send_sms("http://kurecell.com.tr/kurecellapiV2/api-center/", $data);

        return $send_message;
    }

    /**
     * @param $number
     * @param $code
     * @return mixed
     */
    public function sendValidate($number, $code)
    {
        $numbers = array($number);
        $message = 'Onay Kodunuz :'. $code;
        $title   = 'betsikayet';

        $data = array(
            'apiNo'         =>  '1',
            'user'          =>  '5324645883',
            'pass'          =>  'kral00',
            'mesaj'         =>  $message,
            'numaralar'     =>  $numbers,
            'baslik'        =>  $title,
        );

        $send_message = $this->send_sms("http://kurecell.com.tr/kurecellapiV2/api-center/", $data);

        return $send_message;
    }
}