<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WebController@index');

Route::get('/home', 'WebController@index');

#User Routes
Route::match(['get', 'post'], '/login', 'UserController@login');
Route::match(['get', 'post'], '/register', 'UserController@register');
Route::match(['get', 'post'], '/logout', 'UserController@logout');
Route::post('/getuser', 'UserController@getUser');
Route::get('/user/account-detail', 'WebController@getUserAccountDetail');
Route::get('/user/bank_accounts', 'WebController@getUserBankAccounts');
Route::get('/user/finance', 'WebController@getUserFinance');
Route::get('/user/change_password', 'WebController@getUserChangePassword');
Route::post('/user/change_password_update', 'WebController@getUserChangePasswordUpdate');
Route::get('/user/bonus', 'WebController@getUserBonus');
Route::match(['get', 'post'], '/user/bank-add', 'WebController@getUserBankAdd');
Route::match(['get', 'post'], '/user/withdraw', 'WebController@getUserWithDraw');
Route::match(['get', 'post'], '/user/bank_deposits', 'WebController@getUserBankDeposits');
Route::match(['get', 'post'], '/user/cepbank_deposits', 'WebController@getUserCepBankDeposits');

# Slot Routes
Route::get('/slots', 'WebController@getSlotGames');
Route::get('/slot/game/{game_id}/{provider}', 'WebController@getSlotGame');

# Bonus
Route::get('/bonus', 'WebController@getBonus');

# Help
Route::get('/help/deposits', 'WebController@getHelpDeposits');
Route::get('/help/withdraw', 'WebController@getHelpWithdraw');
Route::get('/help/terms-of-use', 'WebController@getHelpTermOfUse');
Route::get('/help/affilate', 'WebController@getHelpAffilate');

# Live Routes
Route::get('/casino' , 'WebController@getLiveGames');
Route::get('/casino/game/{game_id}', 'WebController@getLiveGame');

# xPro Routes
Route::get('/crateAccount', 'xProController@createAccount');
Route::get('/getAccountBalances', 'xProController@getAccountBalances');
Route::get('/gamelist', 'xProController@getGamesListWithLimits');
Route::get('/game/{id}/{provider}', 'CasinoController@getSlotUrl');
Route::get('/close', 'xProController@getAccountBalance');


#Admin
Route::get('/menage', 'AdminController@index');
Route::get('/menage/users', 'AdminController@getUsers');
Route::get('/menage/user/balance/{user_id}', 'AdminController@getBalance');

Route::get('/menage/accounting/deposist', 'AdminController@getDeposits');
Route::get('/menage/setdeposit/{deposit_id}/{process}', 'AdminController@setDeposits');

Route::get('/menage/accounting/draws', 'AdminController@getDraws');
Route::get('/menage/setdraw/{draw_id}/{process}', 'AdminController@setDraw');


Route::get('/menage/getAccounting', 'AdminController@Getaccounting');

Route::match(['get', 'post'], '/menage/balance_add', 'AdminController@insertBalance');
Route::match(['get', 'post'], '/menage/balance_remove', 'AdminController@deleteBalance');


// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


//Kurecell

Route::match(['get', 'post'], '/user/sms_validate', 'UserController@SmsValidate');

Route::get('kurcell/{number}', 'KurecellController@testKureCell');