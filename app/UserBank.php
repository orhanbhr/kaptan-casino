<?php
/**
 * Created by PhpStorm.
 * User: OrhanBHR
 * Website: www.orhanbhr.com
 * Mail: this@orhanbhr.com
 * Date: 13.11.15
 * Time: 02:16
 */


namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_banks';


    public function Transactions()
    {
        return $this->belongsTo('App\Transaction');
    }
}