<?php
/**
 * Created by PhpStorm.
 * User: OrhanBHR
 * Website: www.orhanbhr.com
 * Mail: this@orhanbhr.com
 * Date: 13.11.15
 * Time: 02:16
 */


namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetails extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transaction_details';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}