<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png')}}" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/site.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sky-mega-menu.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/parralax.css')}}" />
    <script type="text/javascript" src="{{ URL::asset('assets/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery-1.11.2.min.js')}}"> </script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/hover_pack.js')}}"></script>



    <link href="{{ URL::asset('assets/css/magic_slider.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tabs/sky-tabs.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hover_pack.css')}}">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/slider/jquery.ui.touch-punch.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/slider/magic_slider.js')}}" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ URL::asset('assets/img/favicon.png')}}assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.placeholder.min.js')}}"></script>
    <![endif]-->



    <title>BuCasino Canlı Casino Paralı Casino Oyna</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="{{URL::asset('assets/img/logoSM.png')}}" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')

<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Cepbank ile Para Yatır</h1>
            <p style="color:#FFF; font-size:20px;">BuCasino hesabınıza cepbank ile para yatırın</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="/">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Cepbank ile Para Yatırma</li>
            </ol>

        </div>
    </section>
</div>

<div class="container howTo">
    <div class="col-md-12 no-padding howTo2 helpContentSM helpContentXS" style="padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; !important; color:#000 !important;">

        <div class="container no-padding">
            <div class="col-md-3" style="background:#fff; padding:1em; border-radius:0em; font-size:16px; color:#000 !important;">

                <ul class="nav nav-pills nav-stacked">


                    <li class="accSidebar">
                        <a href="/user/bank_deposits" class="accSidebar">
                            Havale İle Para Yatır
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/cepbank_deposits" class="accSidebar">
                            Cepbank İle Para Yatır
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                    <li class="accSidebar">
                        <a href="/user/withdraw" class="accSidebar">
                            Para Çek
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                    <li class="accSidebar">
                        <a href="/user/finance" class="accSidebar">
                            Hesap Hareketlerim
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <p style="font-size:22px; font-weight:bold; padding-top:0.5em; border-bottom:1px solid #000;"></p>
                    <li class="accSidebar">
                        <a href="/user/account-detail" class="accSidebar">
                            Üyelik Bilgilerim
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/bonus" class="accSidebar">
                            Bonuslarım
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/bank_accounts" class="accSidebar">
                            Banka Hesaplarım
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/change_password" class="accSidebar">
                            Şifremi Değiştir
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                    <p style="font-size:22px; font-weight:bold; padding-top:0.5em; border-bottom:1px solid #000;"></p>

                    <li class="accSidebar">
                        <a href="/logout" class="accSidebar">
                            Çıkış Yap
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                </ul>

            </div>
            <div class="col-md-1"></div>
            <div class="col-md-8 no-padding" style=" background:#fff; padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; !important; color:#000 !important; overflow:hidden;">


                <h4 style="border:none !important;" class="howToH4 text-center">Cepbank İle Para Yatırma</h4>

                <div class="container">
                    <div class="col-md-8">

                        <h5>Lütfen, aşağıdaki cepbank ile para yatırma talep formunu eksiksiz doldurun.</h5>
                        <ul>
                            <li>Yatırdığınız miktar, talep formunu doldurduktan sonra maksimum 25 dk. içerisinde hesabınıza yatmaktadır.</li>
                            <li>Cepbank ile saat 12.00 ile 05.00 saatleri arasında yatırım yapabilirsiniz.</li>
                            <li>Cepbank ile minimum para yatırma limiti 50 TL'dir.</li>
                        </ul>
                    </div>
                </div>
                <ul class="padd1">
                    <form method="post">
                        <div class="col-md-12 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Banka:</label>
                                <select class="form-control" name="bank_name">
                                    <option value="Garanti Cep Bank">Garanti Cep Bank</option>
                                    <option value="Akbank Cep Cüzdan">Akbank Cep Cüzdan</option>
                                    <option value="Yapı Kredi Cebe havale">Yapı Kredi Cebe Havale</option>
                                    <option value="İş Bankası İş Cep">İş Bankası İş Cep</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">İşlem Yapılan Numara:</label>
                                <input style="border-radius:0; font-size:18px;" type="number" name="sender_phone" class="form-control" id="exampleInputEmail1" placeholder="İşlem Yapılan Numara">
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Gönderim Yapılan Numara:</label>
                                <input style="border-radius:0; font-size:18px;" type="number" name="receiver_phone" class="form-control" id="exampleInputEmail1" placeholder="Gönderim Yapılan Numara">
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">TC Kimlik No:</label>
                                <input style="border-radius:0; font-size:18px;" type="number" name="tc_number" class="form-control" id="exampleInputEmail1" placeholder="TC Kimlik No">
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Telefona Gelen Referans No:</label>
                                <input style="border-radius:0; font-size:18px;" type="number" name="referance_number" class="form-control" id="exampleInputEmail1" placeholder="Telefona Gelen Referans No">
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Yatırdığınız Miktar:</label>
                                <input style="border-radius:0; font-size:18px;" type="number" name="amount" class="form-control" id="exampleInputEmail1" placeholder="Yatırdığınız Miktar">
                            </div>
                        </div>

                        <button style="margin-left:0em !important; margin-top:0em; border:none !important;" type="submit" class="btn btn-block lobiButton" >Para Yatırma Talebi Gönder</button>
                    </form>

            </div>

        </div>
    </div>
</div>

@include('includes.footer')
</body>

</html>