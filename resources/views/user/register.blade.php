<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/site.css" />
    <link rel="stylesheet" href="assets/css/sky-mega-menu.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/parralax.css" />
    <script type="text/javascript" src="assets/js/modal.js"></script>
    <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"> </script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/hover_pack.js"></script>


    <link href="assets/css/magic_slider.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/tabs/sky-tabs.css">
    <link rel="stylesheet" href="assets/css/hover_pack.css">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="assets/js/slider/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="assets/js/slider/magic_slider.js" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="assets/js/jquery.placeholder.min.js"></script>
    <![endif]-->



    <title>BuCasino Canlı Casino Paralı Casino Oyna</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="assets/img/logoSM.png" /></div>
        <div class="col-md-4"></div>
    </div>
</div>

@include('includes.header')


<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Yeni Üyelik</h1>
            <p style="color:#FFF; font-size:20px;">BuCasino'ya hemen üye olun, kazanmaya hemen başlayın!</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="index.html">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Yeni Üyelik</li>
            </ol>

        </div>
    </section>
</div>

<div class="container howTo">
    <div class="col-md-8 no-padding howTo2 helpContentSM helpContentXS" style="background:#fff; padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; background:#fff !important; color:#000 !important;">


        <h4 style="border:none !important;" class="howToH4 text-center">Hemen Üye Olun!</h4>

        @if($errors->any())
            <h4>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Kayıt Hatası!</strong> {{$errors->first()}}.
                </div>

            </h4>
        @endif
        <ul class="padd1">
            <form  method="POST">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Adınız:</label>
                        <input  required style="border-radius:0" type="text" class="form-control" name="first_name" id="exampleInputEmail1" placeholder="Adınız">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Soyadınız:</label>
                        <input required style="border-radius:0" type="text" class="form-control" name="last_name" id="exampleInputPassword1" placeholder="Soyadınız">
                    </div>
                </div>

                <div class="col-md-6">
                    <label for="exampleInputPassword1">Doğum Tarihiniz:</label>
                    <div class="form-group">
                        <div class="col-md-3 no-padding dropdown-appearance">
                            <select required style="font-family:'PT Sans Narrow', Arial, Helvetica, sans-serif;" name="birth_day" class="form-control">
                                <option value="00">Gün</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                        </div>
                        <div class="col-md-3 no-padding dropdown-appearance">
                            <select required style="font-family:'PT Sans Narrow', Arial, Helvetica, sans-serif;" class="form-control" name="birth_month" id="finishMonth" name="bdMonth">
                                <option value="00">Ay</option>
                                <option value="01">Ocak</option>
                                <option value="02">Şubat</option>
                                <option value="03">Mart</option>
                                <option value="04">Nisan</option>
                                <option value="05">Mayıs</option>
                                <option value="06">Haziran</option>
                                <option value="07">Temmuz</option>
                                <option value="08">Ağustos</option>
                                <option value="09">Eylül</option>
                                <option value="10">Ekim</option>
                                <option value="11">Kasım</option>
                                <option value="12">Aralık</option>
                            </select>
                        </div>
                        <div class="col-md-3 no-padding dropdown-appearance">
                            <select required style="font-family:'PT Sans Narrow', Arial, Helvetica, sans-serif;" class="form-control" name="birth_year" id="finishYear" name="bdYear">
                                <option value="00">Yıl</option>
                                <option value="1930">1930</option>
                                <option value="1931">1931</option>
                                <option value="1932">1932</option>
                                <option value="1933">1933</option>
                                <option value="1934">1934</option>
                                <option value="1935">1935</option>
                                <option value="1936">1936</option>
                                <option value="1937">1937</option>
                                <option value="1938">1938</option>
                                <option value="1939">1939</option>
                                <option value="1940">1940</option>
                                <option value="1941">1941</option>
                                <option value="1942">1942</option>
                                <option value="1943">1943</option>
                                <option value="1944">1944</option>
                                <option value="1945">1945</option>
                                <option value="1946">1946</option>
                                <option value="1947">1947</option>
                                <option value="1948">1948</option>
                                <option value="1949">1949</option>
                                <option value="1950">1950</option>
                                <option value="1951">1951</option>
                                <option value="1952">1952</option>
                                <option value="1953">1953</option>
                                <option value="1954">1954</option>
                                <option value="1955">1955</option>
                                <option value="1956">1956</option>
                                <option value="1957">1957</option>
                                <option value="1958">1958</option>
                                <option value="1959">1959</option>
                                <option value="1960">1960</option>
                                <option value="1961">1961</option>
                                <option value="1962">1962</option>
                                <option value="1963">1963</option>
                                <option value="1964">1964</option>
                                <option value="1965">1965</option>
                                <option value="1966">1966</option>
                                <option value="1967">1967</option>
                                <option value="1968">1968</option>
                                <option value="1969">1969</option>
                                <option value="1970">1970</option>
                                <option value="1971">1971</option>
                                <option value="1972">1972</option>
                                <option value="1973">1973</option>
                                <option value="1974">1974</option>
                                <option value="1975">1975</option>
                                <option value="1976">1976</option>
                                <option value="1977">1977</option>
                                <option value="1978">1978</option>
                                <option value="1979">1979</option>
                                <option value="1980">1980</option>
                                <option value="1981">1981</option>
                                <option value="1982">1982</option>
                                <option value="1983">1983</option>
                                <option value="1984">1984</option>
                                <option value="1985">1985</option>
                                <option value="1986">1986</option>
                                <option value="1987">1987</option>
                                <option value="1988">1988</option>
                                <option value="1989">1989</option>
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                                <option value="1994">1994</option>
                                <option value="1995">1995</option>
                                <option value="1996">1996</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Cinsiyetiniz:</label>
                    <select style="font-family:'PT Sans Narrow', Arial, Helvetica, sans-serif;" class="form-control" id="gender" name="sex">
                        <option value="">Seçiniz</option>
                        <option value="1">Bay</option>
                        <option value="2">Bayan</option>
                    </select>
                </div>

                <div class="col-md-6" style="margin-top:1em;">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Telefon Numaranız:</label>
                        <input required style="border-radius:0" type="number" class="form-control" name="phone" id="exampleInputPassword1" placeholder="Telefon Numaranız">
                    </div>
                </div>
                <div class="col-md-6" style="margin-top:1em;">
                    <div class="form-group">
                        <label for="exampleInputPassword1">E-Posta Adresiniz:</label>
                        <input required style="border-radius:0" type="email" name="email" class="form-control" id="exampleInputPassword1" placeholder="E-Posta Adresiniz">
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Yaşadığınız Şehir:</label>
                        <input required style="border-radius:0" type="text" name="location" class="form-control" id="exampleInputPassword1" placeholder="Yaşadığınız Şehir">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kullanıcı Adınız:</label>
                        <input required style="border-radius:0" type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="Kullanıcı Adınız">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Şifreniz:</label>
                        <input required style="border-radius:0" type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Şifreniz">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Şifreniz Tekrar:</label>
                        <input required style="border-radius:0" type="password" name="password_confirmation" class="form-control" id="exampleInputPassword1" placeholder="Şifreniz Tekrar">
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="input-group">
                        <input required type="checkbox" /><i style="margin-left:0.3em !important;">18 Yaşından büyüğüm ve <a style="color:#000;" href="#" target="_blank"> Üyelik Sözleşmesi ve Kullanım Şartları</a>'nı okudum onaylıyorum.</i>
                    </div>
                </div>

                <div class="col-md-12	">
                    <a style="text-decoration:none;" href="#"><button style="margin-left:0em !important; margin-top:2em; border:none !important;" type="submit" class="btn btn-block lobiButton" >Gönder</button></a>
                </div>
            </form>
        </ul>
    </div>


    <div class="col-md-1 no-padding"></div>

    <div class="col-md-3 no-padding howTo2" style="background:#fff; padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; background:#fff !important; color:#000 !important;">

        <h4 style="border:none !important;" class="howToH4 text-center">Hemen Üye Olun</h4>
        <p style=" padding:0 2em 0 2em;">BuCasino.com, yalnızca canlı casino, casino / slot hizmeti veren, casino sitesidir.

            Yalnızca casinoya has bonusları ile, canlı casino dünyasına bambaşka bir boyut getiren BuCasino, canlı casino oyunlarının ne denli heyecanlı ve zevkli bir oyun olduğunu kanıtlayabilmek adına, yalnızca canlı casino hizmeti veren bir casino sitesi misyonunu taşır.</p>

        <p style="padding:0 2em 0 2em; font-size:18px;">500 TL'ye varan %100 ilk para yatırma bonusu, her yatırımınıza özel %10 para yatırma bonusu ve %25 canlı casino discountu!!</p>

        <p style="padding:0 2em 0 2em;  font-size:18px;">Tüm bu bonuslardan yararlanabilmek için yapmanız gereken tek şey hemen üye olmak kazanmaya başlamak!</p>

    </div>
</div>

@include('includes.footer')
</body>

</html>
