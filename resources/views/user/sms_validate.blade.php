<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png')}}" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/site.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sky-mega-menu.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/parralax.css')}}" />
    <script type="text/javascript" src="{{ URL::asset('assets/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery-1.11.2.min.js')}}"> </script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/hover_pack.js')}}"></script>



    <link href="{{ URL::asset('assets/css/magic_slider.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tabs/sky-tabs.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hover_pack.css')}}">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/slider/jquery.ui.touch-punch.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/slider/magic_slider.js')}}" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ URL::asset('assets/img/favicon.png')}}assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.placeholder.min.js')}}"></script>
    <![endif]-->



    <title>BuCasino Canlı Casino Paralı Casino Oyna</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="{{URL::asset('assets/img/logoSM.png')}}" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')

<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Şifremi Unuttum</h1>
            <p style="color:#FFF; font-size:20px;">Sms Doğrulama</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="/">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Sms Doğrulama</li>
            </ol>

        </div>
    </section>
</div>

<div class="container howTo">
    <div class="col-md-12 no-padding howTo2 helpContentSM helpContentXS" style="padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; !important; color:#000 !important;">

        <div class="container no-padding">

            <div class="col-md-1"></div>
            <div class="col-md-10 no-padding" style=" background:#fff; padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; !important; color:#000 !important; overflow:hidden;">

                <h4 style="border:none !important;" class="howToH4 text-center">Sms Doğrulama</h4>

                <ul class="padd1">


                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') == "We have e-mailed your password reset link!" ? 'Şifre sıfırlama linkiniz e-mail adresinize gönderilmiştir.' : session('status') }}
                        </div>
                        @endif
                        <form method="POST" action="/user/sms_validate">
                            {!! csrf_field() !!}

                            @if (count($errors) > 0)
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>
                                            <div class="alert alert-danger">
                                                {{ $error }}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif

                            <div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email Adresiniz:</label>
                                    <input style="border-radius:0; font-size:18px;" type="email" class="form-control"  name="email" id="exampleInputEmail1" placeholder="Email">
                                </div>
                            </div>

                            <div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">SMS Şifreniz</label>
                                    <input style="border-radius:0; font-size:18px;" type="text" class="form-control" name="sms_valid" placeholder="SMS Kodunuz">
                                </div>
                            </div>

                            <div>
                                <button style="margin-left:0em !important; margin-top:0em; border:none !important;" type="submit" class="btn btn-block lobiButton" >Hesabımı Onayla</button>
                            </div>
                        </form>

            </div>

        </div>
    </div>
</div>

@include('includes.footer')
</body>

</html>