<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png')}}" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/site.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sky-mega-menu.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/parralax.css')}}" />
    <script type="text/javascript" src="{{ URL::asset('assets/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery-1.11.2.min.js')}}"> </script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/hover_pack.js')}}"></script>



    <link href="{{ URL::asset('assets/css/magic_slider.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tabs/sky-tabs.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hover_pack.css')}}">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/slider/jquery.ui.touch-punch.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/slider/magic_slider.js')}}" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ URL::asset('assets/img/favicon.png')}}assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.placeholder.min.js')}}"></script>
    <![endif]-->



    <title>BuCasino Canlı Casino Paralı Casino Oyna</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="{{URL::asset('assets/img/logoSM.png')}}" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')

<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Hesap Hareketlerim</h1>
            <p style="color:#FFF; font-size:20px;">BuCasino'daki finans hareketlerinizi görüntüleyin</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="/">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Hesap Hareketlerim</li>
            </ol>

        </div>
    </section>
</div>

<div class="container howTo">
    <div class="col-md-12 no-padding howTo2 helpContentSM helpContentXS" style="padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; !important; color:#000 !important;">

        <div class="container no-padding">
            <div class="col-md-3" style="background:#fff; padding:1em; border-radius:0em; font-size:16px; color:#000 !important;">

                <ul class="nav nav-pills nav-stacked">


                    <li class="accSidebar">
                        <a href="/user/bank_deposits" class="accSidebar">
                            Havale İle Para Yatır
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/cepbank_deposits" class="accSidebar">
                            Cepbank İle Para Yatır
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                    <li class="accSidebar">
                        <a href="/user/withdraw" class="accSidebar">
                            Para Çek
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                    <li class="accSidebar">
                        <a href="/user/finance" class="accSidebar">
                            Hesap Hareketlerim
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <p style="font-size:22px; font-weight:bold; padding-top:0.5em; border-bottom:1px solid #000;"></p>
                    <li class="accSidebar">
                        <a href="/user/account-detail" class="accSidebar ">
                            Üyelik Bilgilerim
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/bonus" class="accSidebar">
                            Bonuslarım
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/bank_accounts" class="accSidebar">
                            Banka Hesaplarım
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>
                    <li class="accSidebar">
                        <a href="/user/change_password" class="accSidebar">
                            Şifremi Değiştir
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                    <p style="font-size:22px; font-weight:bold; padding-top:0.5em; border-bottom:1px solid #000;"></p>

                    <li class="accSidebar">
                        <a href="/logout" class="accSidebar">
                            Çıkış Yap
                            <span class="glyphicon glyphicon-chevron-right pull-right arrowMargin"></span>
                        </a>
                    </li>

                </ul>

            </div>
            <div class="col-md-1"></div>
            <div class="col-md-8 no-padding" style=" background:#fff; padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; !important; color:#000 !important; overflow:hidden;">

                <h4 style="border:none !important;" class="howToH4 text-center">Hesap Hareketlerim</h4>

                <div class="container">
                    <div class="col-md-8">
                    </div>
                </div>
                <ul class="padd1">
                    <form action="/user/finance" method="get">
                        <div class="col-md-4 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Başlangıç Tarihi:</label>
                                <div class="dropdown-appearance">
                                    <select class="form-control" name="start_date_day">
                                        <option value="">Gün</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">&nbsp;</label>
                                <div class="dropdown-appearance">
                                    <select class="form-control" name="start_date_month">
                                        <option value="">Ay</option>
                                        <option value="01">Ocak</option>
                                        <option value="02">Şubat</option>
                                        <option value="03">Mart</option>
                                        <option value="04">Nisan</option>
                                        <option value="05">Mayıs</option>
                                        <option value="06">Haziran</option>
                                        <option value="07">Temmuz</option>
                                        <option value="08">Ağustos</option>
                                        <option value="09">Eylül</option>
                                        <option value="10">Ekim</option>
                                        <option value="11">Kasım</option>
                                        <option value="12">Aralık</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">&nbsp;</label>
                                <div class="dropdown-appearance">
                                    <select class="form-control" name="start_date_year">
                                        <option value="">Yıl</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Bitiş Tarihi:</label>
                                <div class="dropdown-appearance">
                                    <select class="form-control" name="end_date_day">
                                        <option value="">Gün</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">&nbsp;</label>
                                <div class="dropdown-appearance">
                                    <select class="form-control" name="end_date_month">
                                        <option value="">Ay</option>
                                        <option value="01">Ocak</option>
                                        <option value="02">Şubat</option>
                                        <option value="03">Mart</option>
                                        <option value="04">Nisan</option>
                                        <option value="05">Mayıs</option>
                                        <option value="06">Haziran</option>
                                        <option value="07">Temmuz</option>
                                        <option value="08">Ağustos</option>
                                        <option value="09">Eylül</option>
                                        <option value="10">Ekim</option>
                                        <option value="11">Kasım</option>
                                        <option value="12">Aralık</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">&nbsp;</label>
                                <div class="dropdown-appearance">
                                    <select class="form-control" name="end_date_year">
                                        <option value="">Yıl</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="form-group">
                                <label for="exampleInputEmail1">İşlem:</label>
                                <select class="form-control" name="process">
                                    <option value="">Hepsi</option>
                                    <option value="1">Oyuna Giriş</option>
                                    <option value="2">Oyundan Çıkış</option>
                                </select>
                            </div>
                        </div>

                        <input style="margin-left:0em !important; margin-top:0em; border:none !important;" type="submit" class="btn btn-block lobiButton" value="Finans Hareketlerimi Listele" />
                    </form>

                    <table class="table table-bordered text-center" style="margin-top:5em !important; color:#000; background:#fff;">
                        <thead>
                        <tr class="tabloHeader">
                            <th>Tarih</th>
                            <th>İşlem</th>
                            <th>Miktar</th>
                            <th>Mesaj</th>
                        </tr>
                        </thead>
                        <tbody class="tabloContent">
                        @foreach($finances as $finance)
                        <tr>
                            <td>{{$finance->created_at}}</td>
                            <td>{{$finance->balance_type == 1 ? 'Oyuna Giriş' : 'Oyundan Çıkış'}}</td>
                            <td>{{$finance->amount}} TL</td>
                            <td>{{$finance->balance_message}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <?php echo $finances->render() ?>

                    <!--<nav>
                        <ul class="pagination pageNumber">

                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>-->
            </div>

        </div>
    </div>
</div>

@include('includes.footer')
</body>

</html>