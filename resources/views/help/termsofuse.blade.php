<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png')}}" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/site.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sky-mega-menu.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/parralax.css')}}" />
    <script type="text/javascript" src="{{ URL::asset('assets/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery-1.11.2.min.js')}}"> </script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/hover_pack.js')}}"></script>



    <link href="{{ URL::asset('assets/css/magic_slider.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tabs/sky-tabs.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hover_pack.css')}}">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/slider/jquery.ui.touch-punch.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/slider/magic_slider.js')}}" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ URL::asset('assets/img/favicon.png')}}assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.placeholder.min.js')}}"></script>
    <![endif]-->



    <title>BuCasino Canlı Casino Paralı Casino Oyna</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="{{URL::asset('assets/img/logoSM.png')}}" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')
<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Kullanım Şartları</h1>
            <p style="color:#FFF; font-size:20px;">BuCasino Canlı Casino Kullanım Şartları ve Gizlilik</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="index.html">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Kullanım Şartları</li>
            </ol>

        </div>
    </section>
</div>

<div class="container howTo">
    <div class="col-md-12 no-padding howTo2 helpContentSM helpContentXS" style="background:#fff; padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; color:#000 !important;">


        <h4 class="howToH4 text-center">KULLANIM ŞARTLARI</h4>
        <p style="font-weight:bold; font-size:22px;" class="padd1">1. Giriş</p>
        <ul>
            <li>Genel Kurallar ve Şartlar, Gizlilik Politikası, Oyun Kuralları</li>
            <li>Web sayfamızda bulabileceğiniz zaman zaman güncellenebilecek her türlü promosyon, bonuslar, özel kampanyalara ait kurallar ve şartları.</li>
            <li>Yukarıda listelenen tüm kural ve şartların tümü "Kurallar" olarak adlandırılır.</li>
            <li>Oyuncular bulundukları ülkenin internetten bahis yapmakla alakalı çevrimiçi oyun kanunlarını bildiklerini kabul ederler. BuCasino.com, ABD, İsrail, İngiliz Virgin Adaları, Filipinler ve Holanda Antilleri’nden bahis Kabul <u>etmemektedir.</u></li>
            <li>Kurallar ve Şartlar kısmında ‘bahis’ ya da ‘bahisler’ diye atıfta bulunulduğu zaman bu BuCasino.com sitesinin casino, poker ya da diğer bir şans oyunu üzerine yapılan ‘gerçek-para’ ya da ‘eğlence için’ bahsi/bahisleridir. Kurallar ve Şartlar içinde yer alan ‘müşteri’ ya da ‘oyuncu’ atıfı ise, BuCasino.com sitesinde geçerli bir hesap sahibi olan kimse için verilen atıftır.</li>
            <li>Genel Kurallar ve Şartların her hangi bir zamanda değiştirilmesi veya güncellenmesi BuCasino.com’un mutlak takdir yetkisi altındadır.</li>
        </ul>
        <p style="font-weight:bold; font-size:22px;" class="padd1">2. Kullanıcı Hesapları</p>
        <ul>
            <li>Tüm yeni kayıtlar takip edilir. Kayıtlar konusunda tamamıyla titiz davranıldığından emin olmak amacıyla, IP adresleri dâhil kontroller yapılır.</li>
            <li>Oyuncu sitede işlem yapmak için istenen gerekli bilgileri tam olarak sağlayamadığında BuCasino.com hesaptaki bakiyeyi iptal etme hakkına sahiptir. Oyuncunun hesabındaki bakiyeyi iptal eden BuCasino.com hakkında Otorite’ye karşı hiçbir iddiada bulunulamaz.</li>
            <li>ABD, İsrail, İngiliz Virgin Adaları, Filipinler ve Holanda Antilleri’nde ikamet edenlerin hesap açması yasaktır. Bu ülkelerde ikamet eden hesaplardan herhangi bir şekilde bir bahsin kabul edilmesi durumunda (geçici veya belirsiz kalma, vatandaş veya bu ülkelerin IP adreslerini vb kullanan kişilerin), BuCasino.com tamamen kendi takdir yetkisine bağlı olarak, ilgili bahislerin tümünü geçersiz sayma ve sadece para yatırma miktarını iade etme hakkına sahiptir.</li>
            <li>BuCasino.com sadece (!) 18 yaşından büyük ‘yetişkin’, ya da (!!) bulundukları bölgedeki reşit olma yaşını geçen müşterilerin bahislerini kabul eder. Küçük yaşta olan ya da usulsüz hareket edenlerin hesaplarını iptal etme hakkı saklıdır.</li>
            <li>BuCasino.com, müşterinin yaşını ve kimliğini doğrulamak için uygun olduğundan emin olduğu VISA/Mastercard veya da banka açıklamasını veya da fotoğraflı kimlik kartının veya da pasaportunu veya da diğer başka bir resmi kişisel kimliğinin okunaklı bir fotokopisini isteme hakkını saklı tutar.</li>
            <li>BuCasino.com, kullanıcılarının bilgilerini ve belgelerini onaylamak için farklı mevcut kaynakları kullanmaktadır. Onay sürecinin başarısız olduğu durumlarda, sağlanmış bilgi ve belgeler ya tatmin edici değildir, ya da yetersizdir. BuCasino.com, kredi/debit kartı ile yatırılan miktarları iade etme ve ilgili bakiyeyi ona göre ayarlama hakkına sahiptir.</li>
            <li>Müşterilere en az 6 karakter uzunluğunda, hem harf hem de sayı içeren güvenli bir şifre seçmeleri tavsiye edilir. Kullanıcı adına, doğum tarihine vb. benzer bir şifre seçmeleri tavsiye edilmez.</li>
            <li>Müşteri istenen belgeleri iletmeyi reddettiğinde, iletemediğinde veya ilettiği belgelerin kriterlere uymaması (sahte düzenlenen, üzerinde oynama yapılan, güvenlik kontrolünden geçemeyen vb) halinde, BuCasino.com tamamen kendi karar yetkisi ile müşterinin hesabını kapatma ve bütün kazançları geçersiz sayma hakkına sahiptir. Bu durumda müşteri tüm kazandıkları üzerindeki hakkını kaybeder. Müşteriler ayrıca para yatırma miktarlarının da geri ödenmeyebileceğini bilmelidir.</li>
            <li>Müşteriler, kayıtlarında herhangi bir güvenlik kontrolüne ve hesapla ilgili bir talebe yanıt verirken, BuCasino.com’un herhangi bir zamanda verdiği ayrıntı ve bilginin her açıdan tam, doğru, eksiksiz olduğunu ve yanıltıcı olmadığını kabul eder ve onaylar.</li>
            <li>Müşteriler, sadece bir bahis hesabını kaydolup kullanılabilir. Aynı kişisel ya da banka ayrıntılarıyla, IP adresi ya da posta koduyla açılan, var olan her hangi bir hesapla bağlantılı olduğu belirlenen hesaplar anında kapatılır; tüm bahisler iptal edilir ve kazanılanlar BuCasino.com’un kararı uyarınca geri alınır.</li>
            <li>Her müşteri kullanıcı adı ve güvenlik ayrıntılarını güvende ve gizli tutmakla sorumludur. Eğer ihlal şüphesi olursa, müşteriler BuCasino.com kararı uyarınca yeni güvenlik ayrıntıları bulmak, gelecekteki işlemler için önceki ayrıntıları iptal etmek gibi gibi uygun önlemleri hemen almalıdır.</li>
            <li>Her müşteri, para yatırma, bahis ve para çekme gibi giriş ayrıntıları ve hesap kullanılarak yapılan tüm finansal işlemler konusunda sorumludur. Kişisel kayıt ayrıntılarının yetki dışı kullanımı, müşterinin kendi sorumluluğundadır ve Şirketin yeterli fon ve bahis kabul kriterlerini karşılamaya tabi olarak geçerli sayılır.</li>
            <li>Her müşteri BuCasino.com’daki kayıtlarını, özellikle de adrese ödeme ayrıntılarını güncel tutmakla yükümlüdür.</li>
            <li>Oyuncunun hesabına 30 ay boyunca para yatırılmadığında ya da oyuncunun yeri tam olarak belirlenemediğinde, BuCasino.com hesaptaki bakiyeyi iptal etme hakkına sahiptir. Oyuncunun hesabındaki bakiyeyi iptal eden BuCasino.com hakkında Otorite’ye karşı hiçbir iddiada bulunulamaz.</li>
            <li>BuCasino.com müşterilere aşağıdaki eylemleri yapma izni tanımaz:
                <ul>
                    <li>Bahis hesabına kendi kişisel bilgileri dışında ayrıntılar kullanarak kaydolmak;</li>
                    <li>Herhangi bir üçüncü kişi adına hareket etmek;</li>
                    <li>Suç kabul edilen eylemlerden kazanılan parayı yatırmak;</li>
                    <li>Müşterinin kullanma yetkisi olmayan bir kedi kartıyla para yatırmak ya da üçüncü bir tarafla beraber usulsüzlük yaparak bilerek bu tür bir karttan para almak;</li>
                    <li>Kendi hesaplarını ya da üçüncü kişilerin hesabını para aklamak ya da diğer yasadışı amaçlar için kullanmak;</li>
                    <li>Transfer hesabını başkalarına satmak.</li>
                </ul>
            </li>
            <li>Müşterilere her giriş yaptıklarında ya da BuCasino.com ile iletişime geçtiklerinde hesaplarındaki bakiyeyi kontrol etmeleri tavsiye edilir. Tartışma ya da soruşturma durumunda müşteriler ilk fırsatta BuCasino.com’u bakiye son doğrulandığı zamandan sonraki işlem listesi konusunda bilgilendirmekle yükümlüdür.</li>
            <li>Para Aklamanın Önlenmesi adına BuCasino.com uluslararası Para Aklama denetimlerine uymakla yükümlüdür. Bu Yönetmelikler para aklamanın ve terör finansmanının risklerini belirlemek üzere küresel standartları belirlemiştir. BuCasino.com aşağıdakilerin denetlenmesine ihtiyaç duymaktadır.</li>
            <li>Müşteri kimliği, gereken özenin gösterilmesi ve kimlik bilgisi (örneğin belirli kimlik belgelerinin talebinde)</li>
            <li>İşlemlerin müşterilerin risk profiline dair bilgimize uygun olup olmadığından emin olmak için, müşterinin finansal alışkanlıkları ve davranışları</li>
        </ul>
        <p style="font-weight:bold; font-size:22px;" class="padd1">3. Ödemeler</p>
        <ul>
            <li>BuCasino.com’da hesaplara, havale yoluyla cep telefon numarasına gönderme yöntemiyle para yatırılabilmektedir. Yeni para yatırma yöntemleri eklendikçe ana sayfamızda ve e-mail ile bilgilendirme yapılacak, her yöntem eklendiğinde "para yatırma kuralları" da güncellenecektir.</li>
            <li>Cepbank işlemleri, banka hesabınızdan cep telefon numarasına gönderim şeklinde yapılır. Hızlı ve güvenli olan bu yöntem için para yatırma sayfasında belirtilen bankalardaki hesaplarınızdan işlem yapabilirsiniz. Bu para yatırma yöntemiyle ilgili yönlendirici açıklamalar, ilgili para yatırma formu sayfasında yer almaktadır.</li>
            <li>İlgili para yatırma formu tam ve hatasız doldurulduktan itibaren paranın BuCasino.com hesabınıza geçme süresi, 5 dakika ile 15 saat arasında değişebilir. Daha fazla gecikme olduğunda BuCasino.com müşteri hizmetlerine başvurulmalıdır.</li>
            <li>Müşteriler bahis hesaplarına para yatırabilmek için sadece kendi kişisel bilgilerini ve banka hesaplarını kullanmalıdır. Para yatırma formunda belirtilen bilgilerin doğruluğu müşterilerimizin sorumluluğundadır.</li>
            <li>Para yatırma formunda bulunan kişisel bilgilerle, para yatıran üyelerimizin BuCasino.com üyelik bilgileri aynı olmak zorundadır.</li>
            <li>BuCasino.com`da para çekme sayfasında belirtilen bankalara para çekme emri verebilirsiniz. Banka adetleri ilerleyen günlerde artacaktır ve yeni para çekme yöntemleri eklenecektir. Yeni para çekme yöntemleri eklendikçe ana sayfamızda ve e-mail ile bilgilendirme yapılacak, her yöntem eklendiğinde "para çekme kuralları" da güncellenecektir.</li>
            <li>Banka havalesi ile bir seferde minimum para çekme limiti 100 TL’dir. BuCasino.com hesaplarınızdan her takvim günü için limitsiz para çekme emri verebilirsiniz.</li>
            <li>Eğer para çekme talimatınızdaki bilgilerde sorun yoksa tüm ödemeler, para çekme talebinizden itibaren en geç 1 iş günü içinde tamamlanacaktır. İşlem süreleri garanti edilen süreler değildir ve değişik sebeplerden dolayı bu süreler uzayabilir (hesapların ve bilgilerin kontrolü, banka çalışma süreleri, tatiller vs).</li>
            <li>Para çekme talimatı verdiğiniz anda belirttiğiniz tutar hesabınızdan düşülecektir.</li>
            <li>Eğer para çekme talimatınızdaki bilgilerde bir sorun çıkarsa, para çekme talimatı iptal edilecek ve daha önce düşülen tutar aynen hesabınıza geri yatırılacaktır.</li>
            <li>Hesabınızda bonus varsa, Para çekim talimatını bonus şartlarını tamamladıktan sonra verebilirsiniz.</li>
            <li>Hesabınıza yatırmış olduğunuz paranın tümünü kullandıktan sonra çekim talimatı verebilirsiniz.</li>
            <li>Para çekme emirlerini her gün verebilirsiniz.</li>
            <li>Para çekme formunda bulunan kişisel bilgilerle, para çeken üyelerimizin BuCasino.com üyelik bilgileri aynı olmak zorundadır. Başka bir özel veya tüzel kişinin hesabına para talep etmeniz halinde bu işlem yapılmayacaktır.</li>
            <li>BuCasino.com gerektiği durumlarda müşterilerinden ilave belge veya bilgi istemeye, bunlar sağlanmadıkça ödemeleri durdurmaya yetkilidir.</li>
        </ul>
        <p style="font-weight:bold; font-size:22px;" class="padd1">4. Kazançlar</p>
        <ul>
            <li>Poker bürosu hesaplamaları başlangıçta kabul edilen sonuçlar üzerine gerçekleştirir. Sonradan açıklanan diskalifiye ve iptaller hesaplamayı etkilemez.</li>
            <li>Bahisler gerçekleştikten sonra kazançlar oyuncuların hesaplarına aktarılır.</li>
            <li>Müşteriler, hatalı olarak yapılan bir işlem sonucu hesaplarında oluşan tüm ekstra girdileri en kısa sürede bahis şirketine bildirmek durumundadır. Aksi takdirde takip eden ve bu hatayla direkt ya da endirekt bağlantısı olan tüm kazançlar geçersiz olarak değerlendirilip bahis şirketi hesabına geri çekilir.</li>
            <li>Kullanıcının hesabının, daha önce hatalı şekilde kapatılan bir bahsin yeniden hesaplanması sonrasında eksiye düşmesi halinde bu hesap, oyuncu bu negatif tutarı sıfırlayacak ya da bunun üzerine çıkaracak bir para yatırma işlemi yapana kadar bahis şirketi tarafından bloke edilir. Eğer oyuncu böyle bir işlem yapmazsa; Bahis şirketi bu hesap üzerinde şüpheli hesap işlemi yapma hakkına ve yetkisine sahiptir.</li>
            <li>Kullanıcıların para çekmek için kullanacakları tüm yöntemlerdeki hesaplar kendi adlarına olmak zorundadır. Aksi takdirde talepler geçersiz sayılmaktadır.</li>
            <li>Kullanıcı para aktarımı yapmış olduğu yöntem üzerinde iptal talebi vermeme ya da geri ödeme ibrazında bulunmamayı kabul eder. Herhangi bir para yatırma işleminin kullanıcıya iade ve tazmini durumunda bahis şirketi konusu geçen usulsüz para yatırmalar üzerinde masrafları dahil olmak üzere bahis şirketi miktarın ödenmesi sürecini başlatır. Şüpheli bir durumun olmaması için kullanıcının hesabı belirtilen banka hesabı için kullanılamayabilir. Bahis aktivitesi ya da oyunları dışında, kullanıcının banka hesabından bahis şirketine yapılan aktarımlar üzerinde herhangi bir usulsüzlüğün farkına varılması durumunda bahis şirketi bu sebebe yönelik kullanıcıdan ilave ücret talep etme hakkına sahiptir (kullanıcının hesabı kapalı ya da askıya alınmış olsa dahi).</li>
        </ul>
        <p style="font-weight:bold; font-size:22px;" class="padd1">5. Gizlilik</p>
        <ul>
            <li>Web sitemiz tarafından alınan kişisel bilgiler, data güvenlik yasaları gereğince korunmaktadır. Sitemiz kişisel bilgilerin kullanımı konusundaki yükümlülüklerini titizlikle yerine getirmektedir.</li>
            <li>Sitemize verdiğiniz kişisel bilgilerin, site koşulları gereğince web sitemiz tarafından kullanımını ve yasal ve düzenleyici zorunluluklar gereği kullanımı kabul etmiş sayılırsınız.</li>
            <li>Şirket politikası gereği hiçbir kişisel bilgi, hizmet vermekle yükümlü olarak bilgiye erişim hakkı olan firma çalışanlarının dışında, üçüncü şahıslarla paylaşılmamaktadır.</li>
            <li>Sizden aldığımız bilgilerin doğru bir şekilde kaydını sağlamak için bize göndermiş olduğunuz her türlü iletişim bilgisinin kopyası (e-maillerin kopyası da dahil olmak üzere) tarafımızdan saklı tutulacaktır.</li>
        </ul>
    </div>
</div>
<div class="container no-padding">
    <div class="col-md-12">
        <a style="text-decoration:none !important;" href="#"><button style="margin-left:0em !important; margin-top:-2em; border:none !important;" type="button" class="btn btn-block lobiButton" >Hemen Oyna!</button></a>
    </div>
</div>
</div>
</div>

@include('includes.footer')
</body>

</html>