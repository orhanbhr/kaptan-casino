<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png')}}" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/site.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sky-mega-menu.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/parralax.css')}}" />
    <script type="text/javascript" src="{{ URL::asset('assets/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery-1.11.2.min.js')}}"> </script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/hover_pack.js')}}"></script>



    <link href="{{ URL::asset('assets/css/magic_slider.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tabs/sky-tabs.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hover_pack.css')}}">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/slider/jquery.ui.touch-punch.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/slider/magic_slider.js')}}" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ URL::asset('assets/img/favicon.png')}}assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.placeholder.min.js')}}"></script>
    <![endif]-->



    <title>BuCasino Canlı Casino Paralı Casino Oyna</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="{{URL::asset('assets/img/logoSM.png')}}" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')

<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Para Çekme Yardım</h1>
            <p style="color:#FFF; font-size:20px;">BuCasino'da Para Çekme Yardımı</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="index.html">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Para Çekme</li>
            </ol>

        </div>
    </section>
</div>

<div class="container howTo">
    <div class="col-md-12 no-padding howTo2 helpContentSM helpContentXS" style="background:#fff; padding:2em 2em 2em 2em; border-radius:0em; font-size:14px; color:#000 !important;">


        <h4 class="howToH4 text-center">PARA ÇEKME</h4>
        <ul class="padd1">
            <li>En düşük para çekme miktarı 100 TL dir.</li>
            <li>Yalnızca kendi adınıza kayıtlı hesabınıza para çekebilirsiniz.</li>
            <li>Para çekim işlemi gerçekleştirebilmek için, sitemize en az bir kez para yatırmış olmanız gerekir.</li>
            <li>Bilgileri eksik verilmiş bir para çekim talebi, yöneticiler tarafından iptal edilir. Üye iptal durumunda bilgileriniz gözden geçirmelidir. Talebin tekrar iptali durumunda üye Canlı Destek Servisine bağlanmalı veya bilgi@bucasino.com adresine mail atmalıdır.</li>
            <li>Finansbank, Garanti Bankası, Akbank,İş Bankası, Yapıkredi veya Ziraat Bankası'ndan herhangi birini "HESABIM" sayfanızdan sisteme ekleyip çekmek istediğiniz tutarı girerek sisteme tanımlı hesabınıza çekim işlemi gerçekleştirebilirsiniz.</li>
            <li>Para çekim işlemlerinizde el şartı dolmayan bonuslar silinir.</li>
            <li>BuCasino.com para çekim talepleriniz en geç 20 - 45 dakika içerisinde güvenli olarak gerçekleştirmektedir.</li>
        </ul>
    </div>
</div>
<div class="container no-padding">
    <div class="col-md-12">
        <a style="text-decoration:none !important;" href="#"><button style="margin-left:0em !important; margin-top:-2em; border:none !important;" type="button" class="btn btn-block lobiButton" >Hemen Oyna!</button></a>
    </div>
</div>
</div>

@include('includes.footer')
</body>

</html>