<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kaptan Casino</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('admin/css/sb-admin.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ URL::asset('admin/css/plugins/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ URL::asset('admin/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/jq-2.1.4,dt-1.10.8/datatables.min.css"/>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/menage">Kaptan Casino</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{$user->username}} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Çıkış</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <a href="/menage"><i class="fa fa-fw fa-dashboard"></i> Ana Sayfa</a>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Kullanıcılar <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="/menage/users">Kullanıcı Listesi</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#accounting"><i class="fa fa-fw fa-arrows-v"></i> Muhasebe <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="accounting" class="collapse">
                        <li class="dropdown-header">Para Yatırma Çekme İşlemleri</li>
                        <li>
                            <a href="/menage/accounting/deposist">Para Yatırma İşlemleri</a>
                        </li>
                        <li>
                            <a href="/menage/accounting/draws">Para Çekme İşlemleri</a>
                        </li>
                        <li class="dropdown-header">Bakiye İşlemleri</li>
                        <li>
                            <a href="/menage/balance_add">Bakiye Ekle</a>
                        </li>
                        <li>
                            <a href="/menage/balance_remove">Bakiye Çıkar</a>
                        </li>
                        <li class="dropdown-header">Bonus İşlemleri</li>
                        <li>
                            <a href="#">Bonus Tanımla</a>
                        </li>
                        <li class="dropdown-header">Genel</li>
                        <li>
                            <a href="/menage/getAccounting">Genel ( Gelir / Gider )</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Yönetim Paneli
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i> Tüm Kullanıcılar
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">

                    <table class="table table-striped table-bordered table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Kullanıcı Adı</th>
                            <th>Adı</th>
                            <th>Soyadı</th>
                            <th>E-mail</th>
                            <th>Telefon</th>
                            <th>Son Casino Giriş</th>
                            <th>Bakiyesi</th>
                            <th>Üyelik Tarihi</th>
                            <th>Durum</th>
                            <th>İşlemleri</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->casino_last_login }}</td>
                            <td>{{ $user->balance }}</td>
                            <td>{{ $user->created_at }}</td>
                            @if($user->status == 1)
                                <td><span class="label label-success">Aktif</span></td>
                            @else
                                <td><span class="label label-danger">Aktif Değil</span></td>
                            @endif
                            <th><a href="/menage/user/balance/{{$user->id}}" class="btn btn-sm btn-primary">İşlemleri</a></th>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="{{ URL::asset('admin/js/jquery.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('admin/js/bootstrap.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ URL::asset('admin/js/plugins/morris/raphael.min.js')}}"></script>
<script src="{{ URL::asset('admin/js/plugins/morris/morris.min.js')}}"></script>
<script src="{{ URL::asset('admin/js/plugins/morris/morris-data.js')}}"></script>


<script src="https://cdn.datatables.net/r/bs/jq-2.1.4,dt-1.10.8/datatables.min.js"></script>
<script type="text/javascript">
    $('#data-table').dataTable(
            {
                "bProcessing": true,
                "language": { "url": "////cdn.datatables.net/plug-ins/725b2a2115b/i18n/Turkish.json" },
                "order": [ [ 4, "desc" ]],
                "pageLength": 50,
                "lengthMenu": [50, 100, 200, 500, 1000, 10000]
            }
    );
</script>

</body>

</html>
