<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="60">

    <title>Kaptan Casino</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('admin/css/sb-admin.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ URL::asset('admin/css/plugins/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ URL::asset('admin/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/jq-2.1.4,dt-1.10.8/datatables.min.css"/>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/menage">Kaptan Casino</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{$user->username}} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Çıkış</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <a href="/menage"><i class="fa fa-fw fa-dashboard"></i> Ana Sayfa</a>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Kullanıcılar <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="/menage/users">Kullanıcı Listesi</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#accounting"><i class="fa fa-fw fa-arrows-v"></i> Muhasebe <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="accounting" class="collapse">
                        <li class="dropdown-header">Para Yatırma Çekme İşlemleri</li>
                        <li>
                            <a href="/menage/accounting/deposist">Para Yatırma İşlemleri</a>
                        </li>
                        <li>
                            <a href="/menage/accounting/draws">Para Çekme İşlemleri</a>
                        </li>
                        <li class="dropdown-header">Bakiye İşlemleri</li>
                        <li>
                            <a href="/menage/balance_add">Bakiye Ekle</a>
                        </li>
                        <li>
                            <a href="/menage/balance_remove">Bakiye Çıkar</a>
                        </li>
                        <li class="dropdown-header">Bonus İşlemleri</li>
                        <li>
                            <a href="#">Bonus Tanımla</a>
                        </li>
                        <li class="dropdown-header">Genel</li>
                        <li>
                            <a href="/menage/getAccounting">Genel ( Gelir / Gider )</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Para Çekme İşlemleri
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i> Yönetim Paneli
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped table-bordered table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Banka</th>
                            <th>Alıcı Adı Soyadı</th>
                            <th>Iban</th>
                            <th>Kullanıcı</th>
                            <th>Adı Soyadı</th>
                            <th>TC Numarası</th>
                            <th>Miktar</th>
                            <th>Tarih</th>
                            <th>Durum</th>
                            <th>İşlem</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($draws as $draw)
                            <tr>
                                <td>{{ $draw->id }}</td>
                                <td>{{ $draw->user_bank_detail->bank_name }}</td>
                                <td>{{ $draw->user_bank_detail->account_name }}</td>
                                <td>{{ $draw->user_bank_detail->iban }}</td>
                                <td><a href="/menage/user/balance/{{ $draw->user_id }}">{{$draw->user->username}}</a></td>
                                <td>{{ $draw->user->first_name }} {{ $draw->user->last_name }}</td>
                                <td>{{ $draw->user->tc_number }}</td>
                                <td>{{ $draw->amount }}</td>
                                <td>{{ $draw->created_at }}</td>
                                @if($draw->status == 1)
                                    <td><span class="label label-success">Onaylandı</span></td>
                                @elseif($draw->status == 2)
                                    <td><span class="label label-danger">Red Edildi</span></td>
                                @else
                                    <td><span class="label label-warning">Bekliyor</span></td>
                                    <audio width="300" style="display:none;" height="32" src="{{ URL::asset('assets/sounds/alarm.mp3')}}" controls="controls" autoplay="autoplay"></audio>
                                @endif
                                <td><a href="/menage/setdraw/{{$draw->id}}/1" class="btn btn-sm btn-success">Onayla</a> <a href="/menage/setdraw/{{$draw->id}}/2" class="btn btn-sm btn-danger">Red Et</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="{{ URL::asset('admin/js/jquery.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('admin/js/bootstrap.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ URL::asset('admin/js/plugins/morris/raphael.min.js')}}"></script>
<script src="{{ URL::asset('admin/js/plugins/morris/morris.min.js')}}"></script>
<script src="{{ URL::asset('admin/js/plugins/morris/morris-data.js')}}"></script>


<script src="https://cdn.datatables.net/r/bs/jq-2.1.4,dt-1.10.8/datatables.min.js"></script>
<script type="text/javascript">
    $('#data-table').dataTable(
            {
                "bProcessing": true,
                "language": { "url": "////cdn.datatables.net/plug-ins/725b2a2115b/i18n/Turkish.json" },
                "order": [ [ 0, "desc" ]],
                "pageLength": 50,
                "lengthMenu": [50, 100, 200, 500, 1000, 10000]
            }
    );
</script>

</body>

</html>
