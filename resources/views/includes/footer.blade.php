<div class="container-fluid footerBG">
    <div class="container">
        <div class="col-md-12 pull-right">

            <div class="col-md-3">
                <h2 class="footerH2"><img class="footerLogo" src="{{URL::asset('assets/img/footer_logo.png')}}" /></h2>
                <div class="footerLogoBorder"></div>
                <p class="footerSlogan">
                    KaptanCasino.com, ilk ve tek olarak, yalnızca canlı casino hizmeti veren, canlı casino sitesidir.<br /><br />Yalnızca casinoya has bonusları ile, canlı casino dünyasına bambaşka bir boyut getiren bucasino.com, canlı casinonun ne denli heyecanlı ve zevkli bir oyun olduğunu kanıtlayabilmek adına, yalnızca canlı casino hizmeti veren bir casino sitesi misyonunu taşır.
                </p>
            </div>


            <div class="col-md-3">
                <h2 class="footerH2">OYUNLAR</h2>
                <div class="footerLogoBorder"></div>
                <p class="footerParagraph"><a href="/casino">- &nbsp;&nbsp; LIVE ROULETTE</a></p>
                <p class="footerParagraph"><a href="/casino">- &nbsp;&nbsp; LIVE BLACKJACK</a></p>
                <p class="footerParagraph"><a href="/casino">- &nbsp;&nbsp; LIVE BACCARAT</a></p>
                <p class="footerParagraph"><a href="/casino">- &nbsp;&nbsp; LIVE TEXAS HOLD'EM POKER</a></p>
                <p class="footerParagraph"><a href="/casino">- &nbsp;&nbsp; LIVE CARRIBEAN STUD POKER</a></p>
                <p class="footerParagraph"><a href="/slots">- &nbsp;&nbsp; SLOT OYUNLAR</a></p>
            </div>

            <div class="col-md-3">
                <h2 class="footerH2">BONUSLAR</h2>
                <div class="footerLogoBorder"></div>
                <p class="footerParagraph"><a href="/bonus">- &nbsp;&nbsp; %100 İLK PARA YATIRMA BONUSU</a></p>
                <p class="footerParagraph"><a href="/bonus">- &nbsp;&nbsp; %25 CASINO DISCOUNT</a></p>
                <p class="footerParagraph"><a href="/bonus">- &nbsp;&nbsp; %10 PARA YATIRMA BONUSU</a></p>
            </div>

            <div class="col-md-3">
                <h2 class="footerH2">YARDIM</h2>
                <div class="footerLogoBorder"></div>
                <p class="footerParagraph"><a href="/help/deposits">- &nbsp;&nbsp; PARA YATIRMA</a></p>
                <p class="footerParagraph"><a href="/help/withdraw">- &nbsp;&nbsp; PARA ÇEKME</a></p>
                <p class="footerParagraph"><a href="/help/terms-of-use">- &nbsp;&nbsp; KULLANIM ŞARTLARI & GİZLİLİK</a></p>
                <p class="footerParagraph"><a href="/help/sss">- &nbsp;&nbsp; SIKÇA SORULAN SORULAR</a></p>
                <p class="footerParagraph"><a href="/help/affilate">- &nbsp;&nbsp; BAYİLİK</a></p>

            </div>
        </div>
    </div>
</div>
<div class="container-fluid no-padding footerBG2">
    <div class="container no-padding">
        <div class="col-md-4 no-padding">
        </div>
        <div class="col-md-4 no-padding text-center">
            <p class="footerSlogan">Copyright © 2015 KaptanCasino.com</p>
        </div>
        <div class="col-md-4 no-padding">
        </div>
    </div>
</div>

<script src="{{URL::asset('assets/js/jquery.marquee.min.js')}}"></script>
<script src="{{URL::asset('assets/js/breakingnews.js')}}"></script>
<script src="{{URL::asset('assets/js/custom.js')}}"></script>
<script>
    ajaxd();
    $(document).ready(function() {
        setInterval("ajaxd()",5000);
    });

    function ajaxd() {
        $.post( "/getuser", function( data ) {
            $('.balance').text(data.balance);
        });
    }
</script>