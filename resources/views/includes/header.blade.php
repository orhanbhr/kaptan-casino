@if(isset($_REQUEST['failed']))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Giriş Hatası!</strong> Giriş bilgilerinizi kontrol ederek tekrar deneyiniz.
    </div>
@endif

<div class="container-fluid no-padding" style="background:#000 !important;">

    <div class="body container">
        <ul class="sky-mega-menu sky-mega-menu-anim-scale sky-mega-menu-response-to-switcher">

            <li class="switcher">
                <a href="#"><i class="fa fa-bars"></i>Menü</a>
            </li>
            <li class="logoNew visible-lg visible-md" aria-haspopup="true" style="border:none !important;">
                <a href="/"><img src="{{URL::asset('assets/img/index.png')}}" class="logoNew" /></a>
            </li>
            <li aria-haspopup="true" style="border-left:1px solid #F00;">
                <a href="/">Anasayfa</a>
            </li>
            <li aria-haspopup="true">
                <a href="/slots">Casino</a>
            </li>
            <li aria-haspopup="true">
                <a href="/casino">Canlı Casino</a>
            </li>
            <li aria-haspopup="true">
                <a href="/bonus">Bonuslar</a>
            </li>
            <li aria-haspopup="true">
                <a href="#">Yardım</a>
                <div class="grid-container3">
                    <ul>
                        <li style="border-bottom:1px solid #000 !important;"><a href="/help/deposits">Para Yatırma</a></li>
                        <li style="border-bottom:1px solid #000 !important;"><a href="/help/withdraw">Para Çekme</a></li>
                        <li style="border-bottom:1px solid #000 !important;"><a href="/help/terms-of-use">Kullanım Şartları</a></li>
                        <li style="border-bottom:1px solid #000 !important;"><a href="/help/affilate">Bayilik</a></li>
                        <li style="border-bottom:1px solid #000 !important;"><a href="#">Canlı Yardım</a></li>
                    </ul>
                </div>
            </li>
            @if(isset($user) && $user)
                <li aria-haspopup="true" class="right" style="border-right:1px solid #f00;">
                    <a href="#"><i class="fa fa-try"></i><span class="balance">{{$user->balance}}</span></a>
                    <div class="grid-container3">
                        <ul>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/bank_deposits">Para Yatır (Havale)</a></li>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/cepbank_deposits">Para Yatır (Cep Bank)</a></li>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/withdraw">Para Çek</a></li>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/finance">Hesap Hareketlerim</a></li>
                        </ul>
                    </div>
                </li>
                <li aria-haspopup="true" class="right" style="border-right:1px solid #f00;">
                    <a href="#"><i class="fa fa-user"></i> Hesabım</a>
                    <div class="grid-container3">
                        <ul>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/account-detail">Üyelik Bilgilerim</a></li>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/bonus">Bonuslarım</a></li>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/bank_accounts">Banka Hesaplarım</a></li>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/user/change_password">Şifremi Değiştir</a></li>
                            <li style="border-bottom:1px solid #000 !important;"><a href="/logout">Güvenli Çıkış</a></li>
                        </ul>
                    </div>
                </li>
            @else
                <li aria-haspopup="true" class="right">
                    <a href="#_">Giriş Yap</a>
                    <div class="grid-container4">
                        <form action="/login" method="POST">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <fieldset>
                                <section>
                                    <label class="input">
                                        <i class="fa fa-append fa-user"></i>
                                        <input type="text"  name="email" placeholder="Kullanıcı Adınız">
                                    </label>
                                </section>

                                <section>
                                    <label class="input">
                                        <i class="fa fa-append fa-lock"></i>
                                        <input type="password" name="password" placeholder="Şifreniz">
                                    </label>
                                </section>

                                <button type="submit" class="btn btn-primary btn-lg btn-block userLogin">Giriş Yap</button>
                                <button type="button" class="btn btn-primary btn-lg btn-block userLogin"><a style="color:#000;" href="/password/email">Şifremi Unuttum!</a></button>
                            </fieldset>
                        </form>
                    </div>
                </li>
                <li class="right" style="border-right:1px solid #F00;">
                    <a href="/register">Yeni Üyelik</a>
                </li>
            @endif
        </ul>
        <!--/ mega menu -->
    </div>
</div>