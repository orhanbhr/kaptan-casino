<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png')}}" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/site.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sky-mega-menu.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/parralax.css')}}" />
    <script type="text/javascript" src="{{ URL::asset('assets/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery-1.11.2.min.js')}}"> </script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/hover_pack.js')}}"></script>



    <link href="{{ URL::asset('assets/css/magic_slider.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tabs/sky-tabs.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hover_pack.css')}}">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/slider/jquery.ui.touch-punch.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/slider/magic_slider.js')}}" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ URL::asset('assets/img/favicon.png')}}assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.placeholder.min.js')}}"></script>
    <![endif]-->



    <title>{{\App\Http\Controllers\WebController::getTitle()}}</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="{{URL::asset('assets/img/logoSM.png')}}" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')

<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Casino / Slot</h1>
            <p style="color:#FFF; font-size:20px;">Casino / Slot Heyecanını BuCasino farkı ile yaşamaya hazır mısınız?</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="index.html">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Casino</li>
            </ol>

        </div>
    </section>
</div>

<div class="col-md-2"></div>
<div class="col-md-8">
    <div style="padding-top:50px">
        <iframe src="{{$game_url}}" width="100%" frameborder="0" scrolling="no" height="690px"></iframe>
    </div>
</div>
<div class="col-md-2"></div>

<div class="container-fluid footerBG">
    <div class="container">
        <div class="col-md-12 pull-right">

            <div class="col-md-3">
                <h2 class="footerH2"><img class="footerLogo" src="{{ URL::asset('assets/img/footer_logo.png')}}" /></h2>
                <div class="footerLogoBorder"></div>
                <p class="footerSlogan">
                    Bucasino.com, ilk ve tek olarak, yalnızca canlı casino hizmeti veren, canlı casino sitesidir.<br /><br />Yalnızca casinoya has bonusları ile, canlı casino dünyasına bambaşka bir boyut getiren bucasino.com, canlı casinonun ne denli heyecanlı ve zevkli bir oyun olduğunu kanıtlayabilmek adına, yalnızca canlı casino hizmeti veren bir casino sitesi misyonunu taşır.
                </p>
            </div>


            <div class="col-md-3">
                <h2 class="footerH2">OYUNLAR</h2>
                <div class="footerLogoBorder"></div>
                <p class="footerParagraph"><a href="liveCasino.html">- &nbsp;&nbsp; LIVE ROULETTE</a></p>
                <p class="footerParagraph"><a href="liveCasino.html">- &nbsp;&nbsp; LIVE BLACKJACK</a></p>
                <p class="footerParagraph"><a href="liveCasino.html">- &nbsp;&nbsp; LIVE BACCARAT</a></p>
                <p class="footerParagraph"><a href="liveCasino.html">- &nbsp;&nbsp; LIVE TEXAS HOLD'EM POKER</a></p>
                <p class="footerParagraph"><a href="liveCasino.html">- &nbsp;&nbsp; LIVE CARRIBEAN STUD POKER</a></p>
                <p class="footerParagraph"><a href="casino.html">- &nbsp;&nbsp; SLOT OYUNLAR</a></p>
            </div>

            <div class="col-md-3">
                <h2 class="footerH2">BONUSLAR</h2>
                <div class="footerLogoBorder"></div>
                <p class="footerParagraph"><a href="bonus.html">- &nbsp;&nbsp; %100 İLK PARA YATIRMA BONUSU</a></p>
                <p class="footerParagraph"><a href="bonus.html">- &nbsp;&nbsp; %25 CASINO DISCOUNT</a></p>
                <p class="footerParagraph"><a href="bonus.html">- &nbsp;&nbsp; %10 PARA YATIRMA BONUSU</a></p>
            </div>

            <div class="col-md-3">
                <h2 class="footerH2">YARDIM</h2>
                <div class="footerLogoBorder"></div>
                <p class="footerParagraph"><a href="pyYardim.html">- &nbsp;&nbsp; PARA YATIRMA</a></p>
                <p class="footerParagraph"><a href="pcYardim.html">- &nbsp;&nbsp; PARA ÇEKME</a></p>
                <p class="footerParagraph"><a href="sartlar.html">- &nbsp;&nbsp; KULLANIM ŞARTLARI & GİZLİLİK</a></p>
                <p class="footerParagraph"><a href="sss.html">- &nbsp;&nbsp; SIKÇA SORULAN SORULAR</a></p>
                <p class="footerParagraph"><a href="bayilik.html">- &nbsp;&nbsp; BAYİLİK</a></p>

            </div>
        </div>
    </div>
</div>
<div class="container-fluid no-padding footerBG2">
    <div class="container no-padding">
        <div class="col-md-4 no-padding">
        </div>
        <div class="col-md-4 no-padding text-center">
            <p class="footerSlogan">Copyright © 2015 Bucasino.com</p>
        </div>
        <div class="col-md-4 no-padding">
        </div>
    </div>
</div>


<script src="{{ URL::asset('assets/js/jquery.marquee.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/breakingnews.js')}}"></script>
<script src="{{ URL::asset('assets/js/custom.js')}}"></script>
</body>

</html>
