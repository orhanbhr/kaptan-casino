<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/site.css" />
    <link rel="stylesheet" href="assets/css/sky-mega-menu.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/parralax.css" />

    <script type="text/javascript" src="assets/js/modal.js"></script>
    <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"> </script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/hover_pack.js"></script>


    <link href="assets/css/magic_slider.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/tabs/sky-tabs.css">
    <link rel="stylesheet" href="assets/css/hover_pack.css">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="assets/js/jquery.placeholder.min.js"></script>
    <![endif]-->



    <title>{{\App\Http\Controllers\WebController::getTitle()}}</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="assets/img/logoSM.png" /></div>
        <div class="col-md-4"></div>
    </div>
</div>

@include('includes.header')

<div class="container-fluid no-padding" style="margin-top:0em;">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <a href="/bonus"><img src="assets/img/tavlaYeni.jpg" alt="..."></a>
            </div>
            <div class="item">
                <a href="/bonus"><img src="assets/img/disc.jpg" alt="..."></a>
            </div>
            <div class="item">
                <a href="/bonus"><img src="assets/img/pyBonus.jpg" alt="..."></a>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>



<div class="container-fluid no-padding lobiNew">
    <div class="text-center slogan">
        İŞİMİZ GÜCÜMÜZ <b>SADECE CASINO!</b>
    </div>
</div>
<div class="container no-padding">
    <div class="col-md-6 no-padding"></div>
    <div class="col-md-2 no-padding"><div class="ucgen"></div></div>
    <div class="col-md-4 no-padding"></div>
</div>

<div class="container-fluid" style="background:url(assets/img/casino-background.jpg);">
    <div class="container no-padding visible-lg visible-md" style=" margin-bottom:1em;">
        <div class="col-md-12 no-padding">
            <div class="col-md-1 no-padding"></div>

            <div class="col-md-10 contentArea">
                <div class="col-md-4 contentSlogan contentHeadMD">HEMEN ÜYE OLUN
                    <img class="contentSloganArrows" src="assets/img/sloganArrow.png" />
                    <p class="contentSloganPar contentSloganParMD">Her biri birbirinden sexy kurpiyerler</p>

                    <div class="row contentThumbStyle">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/roulette.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Roulette</h3>
                                <p class="contentPar text-center">Birbirinden sexy kurpiyerler ile evinizin rahatlığında gerçek rulet keyfini yaşayın</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>

                    <div class="row contentThumbStyle contentThumb2">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/bj.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Blackjack</h3>
                                <p class="contentPar text-center">Blackjack keyfi Bucasino'da! Evinizin rahatlığında gerçek Blackjack keyfini yaşayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>

                    <div class="row contentThumbStyle contentThumb2">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/texas.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Live Hold'em</h3>
                                <p class="contentPar text-center">Canlı Texas Hold'em keyfi Bucasino'da! Evinizin rahatlığında gerçek Canlı Texas Hold'em keyfini yaşayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 contentSlogan contentHeadMD">BONUSLARI ALIN
                    <img class="contentSloganArrows" style="margin-left:0.2em !important;" src="assets/img/sloganArrow.png" />
                    <p class="contentSloganPar contentSloganParMD">Birbirinden Cömert Bonuslar Kazanın!</p>

                    <div class="row contentThumbStyle">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/autoRoulette.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Auto Roulette</h3>
                                <p class="contentPar text-center">Oto Rulet keyfi Bucasino'da! Evinizin rahatlığında gerçek oto rulet keyfini yaşayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>

                    <div class="row contentThumbStyle contentThumb2">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/baccarat.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Baccarat</h3>
                                <p class="contentPar text-center">Baccarat keyfi Bucasino'da! Evinizin rahatlığında gerçek Baccarat keyfini yaşayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>
                    <div class="row contentThumbStyle contentThumb2">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/carribean.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Carribean Poker</h3>
                                <p class="contentPar text-center">Carribean Poker keyfi Bucasino'da! Evinizin rahatlığında gerçek Carribean Poker keyfini yaşayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>


                </div>

                <div class="col-md-4 contentSlogan contentHeadMD">OYUNA BAŞLAYIN
                    <img class="contentSloganArrows" src="assets/img/sloganArrow.png" />
                    <p class="contentSloganPar contentSloganParMD">Evinizin Rahatlığında Casino Keyfi!</p>

                    <div class="row contentThumbStyle">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/dragon.jpg"  alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Dragon Tiger</h3>
                                <p class="contentPar text-center">Dragon Tiger Rulet keyfi Bucasino'da! Evinizin rahatlığında gerçek Dragon Tiger keyfini yaşayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>

                    <div class="row contentThumbStyle contentThumb2">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/sicBo.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Sic Bo</h3>
                                <p class="contentPar text-center">Sic Bo keyfi Bucasino'da! Evinizin rahatlığında gerçek Sic Bo keyfini yaşayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="liveCasino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>

                    <div class="row contentThumbStyle contentThumb2">
                        <div class="thumbnail contentThumb">
                            <img src="assets/img/slot.jpg" alt="...">
                            <div class="caption">
                                <h3 class="contentHead text-center">Slot Oyunlar</h3>
                                <p class="contentPar text-center">Slot oyunların keyfi Bucasino'da! Evinizin rahatlığında gerçek slot oyun keyfini çıkarmaya hemen başlayın!</p>
                            </div>
                            <a style="text-decoration:none !important;" href="casino.html"><button style="margin-left:0em !important; margin-top:0em;" type="button" class="btn btn-block lobiButton" >HEMEN OYNA</button></a>
                        </div>
                    </div>

                </div>
                <hr />

            </div>
        </div>
    </div>  </div>

@include('includes.footer')
</body>

</html>