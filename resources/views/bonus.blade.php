<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png')}}" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/site.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sky-mega-menu.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/parralax.css')}}" />
    <script type="text/javascript" src="{{ URL::asset('assets/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery-1.11.2.min.js')}}"> </script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/hover_pack.js')}}"></script>



    <link href="{{ URL::asset('assets/css/magic_slider.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tabs/sky-tabs.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hover_pack.css')}}">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/slider/jquery.ui.touch-punch.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/js/slider/magic_slider.js')}}" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ URL::asset('assets/img/favicon.png')}}assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.placeholder.min.js')}}"></script>
    <![endif]-->



    <title>{{\App\Http\Controllers\WebController::getTitle()}}</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="{{URL::asset('assets/img/logoSM.png')}}" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')

<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Cömert Bonuslar</h1>
            <p style="color:#FFF; font-size:20px;">Canlı Casino Heyecanını Bu Casino farkı ile yaşamaya hazır mısınız ?</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="index.html">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Bonuslar</li>
            </ol>

        </div>
    </section>
</div>

<div class="container-fluid">
    <div class="container no-padding bonuslar">
        <div class="col-md-7"><img class="img-responsive" src="assets/img/tavlaYeni.jpg" alt="..."></div>
        <div class="col-md-5 bonuslarContent">
            <h3 class="text-center">%100 İlk Para Yatırma Bonusu</h3>
            <p>Yeni üyelerimize Havale veya Cepbank ile ilk para yatırmalarında 500 TL'ye kadar %100 bonus.</p>
            <ul>
                <li>Canlı casinoda bonusu çekebilmeniz için bonus miktarının 40 katı kadar Rulet oyunlarında ya da 80 katı kadar Blackjack veya Hold'em canlı casino oyunu oynamanız gerekmektedir.</li>
                <li>Bonus miktarı hariç yatırdığınız veya kazandığınız parayı istediğiniz zaman çekebilirsiniz.</li>
            </ul>
        </div>
    </div>
    <div class="container no-padding bonuslar">
        <div class="col-md-7"><img class="img-responsive" src="assets/img/disc.jpg" alt="..."></div>
        <div class="col-md-5 bonuslarContent">
            <h3 class="text-center">%25 Discount (Geri Ödeme)</h3>
            <p>00.00'dan sonra kaybınızın %25'ini nakit olarak alabilirsiniz.</p>
            <ul>
                <li>Casino Discount'unu direk çekebilir ya da herhangi bir bölümde kullanabilirsiniz.</li>
                <li>Discount alabilmek için toplamda minimum 100 TL kaybetmiş olmanız gerekir.</li>
                <li>Gün içerisinde çekim yapanlar bu bonustan <u>yararlanamazlar.</u></li>
            </ul>
        </div>
    </div>
    <div class="container no-padding bonuslar">
        <div class="col-md-7"><img class="img-responsive" src="assets/img/pyBonus.jpg" alt="..."></div>
        <div class="col-md-5 bonuslarContent">
            <h3 class="text-center">%10 Para Yatırma Bonusu</h3>
            <p>Havale ve Cep Bank ile yaptığınız her yatırımda %10 Para Yatırma Bonusu otomatik olarak hesabınıza işleyecektir.</p>
            <ul>
                <li>Bonus miktarı maksimum 500 TL dir.</li>
                <li>Bonusu çekebilmeniz için gerekli el sayısı 500 el oyun şartı gereklidir.</li>
                <li>Bonus miktarı hariç yatırdığınız veya kazandığınız parayı istediğiniz zaman çekebilirsiniz.</li>
            </ul>
        </div>
    </div>
</div>


@include('includes.footer')
</body>

</html>