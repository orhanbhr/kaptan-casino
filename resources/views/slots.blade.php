<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/site.css" />
    <link rel="stylesheet" href="assets/css/sky-mega-menu.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/parralax.css" />
    <script type="text/javascript" src="assets/js/modal.js"></script>
    <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"> </script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/hover_pack.js"></script>



    <link href="assets/css/magic_slider.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/tabs/sky-tabs.css">
    <link rel="stylesheet" href="assets/css/hover_pack.css">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="assets/js/slider/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="assets/js/slider/magic_slider.js" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/css/sky-mega-menu-ie8.css">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="assets/js/jquery.placeholder.min.js"></script>
    <![endif]-->



    <title>{{\App\Http\Controllers\WebController::getTitle()}}</title>
</head>



<body>
<div style="background:#000; padding-top:1em; padding-bottom:1em;" class="container-fluid visible-xs visible-sm">
    <div class="col-md-12 no-padding">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-sm-6"><img src="assets/img/logoSM.png" /></div>
        <div class="col-md-4"></div>
    </div>
</div>


@include('includes.header')


<div class="container-fluid no-padding  parralaxMargin">
    <section class="homeParallaxhelp" data-speed="4" data-type="background">
        <div class="container parallaxSlogan no-padding">
            <h1>Casino / Slot</h1>
            <p style="color:#FFF; font-size:20px;">Casino / Slot Heyecanını BuCasino farkı ile yaşamaya hazır mısınız?</p>
            <ol class="breadcrumb breadcrumbStyle pull-right">
                <li><a href="index.html">Anasayfa</a></li>
                <li class="active breadcrumbStyleColor">Casino</li>
            </ol>

        </div>
    </section>
</div>

<!-- tabs -->
<div style="margin-top:2em !important;" class="container no-padding">
    <div style="overflow:hidden; background:#000 !important;" class="sky-tabs sky-tabs-pos-left sky-tabs-anim-slide-right sky-tabs-response-to-icons">
        <input type="radio" name="sky-tabs" checked id="sky-tab1" class="sky-tab-content-1">
        <label for="sky-tab1"><span><span><i class="fa fa-arrow-right"></i>Slots</span></span></label>

        <input type="radio" name="sky-tabs" id="sky-tab2" class="sky-tab-content-2">
        <label for="sky-tab2"><span><span><i class="fa fa-arrow-right"></i>Table</span></span></label>

        <input type="radio" name="sky-tabs" id="sky-tab3" class="sky-tab-content-3">
        <label for="sky-tab3"><span><span><i class="fa fa-arrow-right"></i>Video Poker</span></span></label>

        <input type="radio" name="sky-tabs" id="sky-tab4" class="sky-tab-content-4">
        <label for="sky-tab4"><span><span><i class="fa fa-arrow-right"></i>Poker</span></span></label>

        <input type="radio" name="sky-tabs" id="sky-tab5" class="sky-tab-content-5">
        <label for="sky-tab5"><span><span><i class="fa fa-arrow-right"></i>Soft Games</span></span></label>

        <ul>

            <li style="padding-top:0px !important; background:#000 !important;" class="sky-tab-content-1">

                @if(isset($games['other_games']))
                    @if(count($games['other_games']) > 0)
                        @foreach($games['other_games'] as $game)
                            @if(!empty($game['image_url']))
                                <div class="col-md-3 no-padding">
                                    <div class="thumbnail oyunlar slotBox">
                                        <a href="#"><h4 class="slotsTypo">{{$game['game_name']}}</h4></a>
                                        <a href="/slot/game/{{$game['game_id']}}/{{$game['provider']}}">
                                            <img class="img-responsive casinoPic" src="{{$game['image_url']}}" />
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endif

            </li>

            <li style="padding-top:0px !important; background:#000 !important;" class="sky-tab-content-2">

                @if(isset($games['table_games']))
                    @if(count($games['table_games']) > 0)
                        @foreach($games['table_games'] as $game)
                            @if(!empty($game['image_url']))
                                <div class="col-md-3 no-padding">
                                    <div class="thumbnail oyunlar slotBox">
                                        <a href="#"><h4 class="slotsTypo">{{$game['game_name']}}</h4></a>
                                        <a href="/slot/game/{{$game['game_id']}}/{{$game['provider']}}">
                                            <img class="img-responsive casinoPic" src="{{$game['image_url']}}" />
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endif

            </li>

            <li style="padding-top:0px !important; background:#000 !important;" class="sky-tab-content-3">
                @if(isset($games['video_poker']))
                    @if(count($games['video_poker']) > 0)
                        @foreach($games['video_poker'] as $game)
                            @if(!empty($game['image_url']))
                                <div class="col-md-3 no-padding">
                                    <div class="thumbnail oyunlar slotBox">
                                        <a href="#"><h4 class="slotsTypo">{{$game['game_name']}}</h4></a>
                                        <a href="/slot/game/{{$game['game_id']}}/{{$game['provider']}}">
                                            <img class="img-responsive casinoPic" src="{{$game['image_url']}}" />
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endif
            </li>

            <li style="padding-top:0px !important; background:#000 !important;" class="sky-tab-content-4">
                @if(isset($games['poker']))
                    @if(count($games['poker']) > 0)
                        @foreach($games['poker'] as $game)
                            @if(!empty($game['image_url']))
                                <div class="col-md-3 no-padding">
                                    <div class="thumbnail oyunlar slotBox">
                                        <a href="#"><h4 class="slotsTypo">{{$game['game_name']}}</h4></a>
                                        <a href="/slot/game/{{$game['game_id']}}/{{$game['provider']}}">
                                            <img class="img-responsive casinoPic" src="{{$game['image_url']}}" />
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endif
            </li>

            <li style="padding-top:0px !important; background:#000 !important;" class="sky-tab-content-5">
                @if(isset($games['soft_games']))
                    @if(count($games['soft_games']) > 0)
                        @foreach($games['soft_games'] as $game)
                            @if(!empty($game['image_url']))
                                <div class="col-md-3 no-padding">
                                    <div class="thumbnail oyunlar slotBox">
                                        <a href="#"><h4 class="slotsTypo">{{$game['game_name']}}</h4></a>
                                        <a href="/slot/game/{{$game['game_id']}}/{{$game['provider']}}">
                                            <img class="img-responsive casinoPic" src="{{$game['image_url']}}" />
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endif
            </li>


        </ul>
    </div>
</div>
<!--/ tabs -->

@include('includes.footer')
</body>

</html>
